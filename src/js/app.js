import './styles'
import './modules'
import States from './states'

angular.module('jumpdigital',[
'ngResource',
'ngAnimate',
'ui.router',
'ui.bootstrap',
'toaster',
'ngFileUpload',
'login',
'change-password',
'angularUtils.directives.dirPagination',
'chart.js',
'datePicker',
'ui.select',
'ngSanitize',
'ngInputModified',
'home',
'common',
'login',
'forgot',
'setting',
'product',
'product_category',
'customers',
'order',
'ecommerce',
'subcategory',
'promo',
'ngCsv',
'ngTableToCsv',
'angular.filter',
'common',

'angular-loading-bar',
'cfp.loadingBar',
'ngTagsInput',
'report',
'ngMaterial',
'banner',
])
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {

  cfpLoadingBarProvider.includeSpinner = false; // Show the spinner.
  cfpLoadingBarProvider.includeBar = true; // Show the bar.
  // cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
  cfpLoadingBarProvider.latencyThreshold = 300;

  //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
  // cfpLoadingBarProvider.loadingBarTemplate = '<div class="page-loader"></div>'
  // cfpLoadingBarProvider.loadingBarTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';

}])
.config(InterceptorConfig)
.config(States)
.config(['paginationTemplateProvider', function(paginationTemplateProvider) {
     paginationTemplateProvider.setString(require('js/common/templates/_paginate.jade'));
}])
.run(initialize)

InterceptorConfig.$inject = [
  '$httpProvider'
]

function InterceptorConfig($httpProvider) {
  $httpProvider.interceptors.push([
    '$injector',
    '$q',
    ($injector, $q) => ({
      'request': config => {
        const AuthService = $injector.get('Auth')
        config.headers['Authorization'] = AuthService.getToken()

        return config
      },

      'requestError': rejection => $q.reject(rejection),
      'response': response => response,
      'responseError': response => $q.reject(response)
      // 'responseError': response => {
      //   const stateService = $injector.get('$state')
      //   if (response.status == 401) stateService.go('login')
      //   return $q.reject(response)
      // }
    })
  ])
}

initialize.$inject = [
  '$anchorScroll',
  '$rootScope',
  '$state',
  'Auth',
  'User',
  'Currency',
  'UnitSystem',
  'StaticData',
  'TimeZones',
  'cfpLoadingBar'
]

function initialize($anchorScroll,$rootScope, $state, Auth, User,Currency, UnitSystem, StaticData,TimeZones,cfpLoadingBar ) {
  StaticData.setCountries();
  $rootScope.util = {}
  const guestStates = [
    'forgot','login'
  ]

  $rootScope.$on('cfpLoadingBar:loaded', () => {
    document.body.style.cursor='default'
    $anchorScroll()
  })
  $rootScope.$on('cfpLoadingBar:started', () => {
    document.body.style.cursor='wait'
  })

  
  $rootScope.$on('$stateChangeStart', (event, toState, toParams, from,fromParams) => {   
    if (toState.redirectTo) {
        event.preventDefault();
        $state.go(toState.redirectTo, toParams, {location: 'replace'})
      }
    $anchorScroll()
    Currency.setCurrency()
    Currency.setCurrentCurrency(Auth.getCurrentCurrency())
    TimeZones.setTimezones()
    TimeZones.setCurrentTimeZone(Auth.getCurrentTimeZone())
    UnitSystem.setCurrentUnitsystem(Auth.getWeightUnit())
    let userToken = null
    let userType = null
    const states = guestStates
    // var isLogin = toState.name === "login";
    //     if(isLogin){
    //        return; // no need to redirect
    //     }
    // if (states.indexOf(toState.name) === -1) return        // current 404 handler
    if ($rootScope.stateChangeBypass) {
      $rootScope.stateChangeBypass = false
      return
    }
    User.getCurrentUser({}, data => {
      $rootScope.util.currentUser = () => data

        userType = Auth.getAuthType()
        userToken = Auth.getToken()

      // check if there's an authToken and authUserType
      if (userToken) {
        if(Auth.getGeneralSettingId() == 'null'){
          $state.go('setting',toParams)
        }
        if(toState.name === "login"){
          $state.go('home',toParams)
        }
        // let nextStateName = (guestStates.indexOf(toState.name) !== -1) ? toState.name : 'login'
         $rootScope.stateChangeBypass = toState.name
        $state.go(toState.name,toParams)
      } else {
        event.preventDefault()
        // if toState is in the list of guestStates, go to that state
        // otherwise redirect to login page
        let nextStateName = (guestStates.indexOf(toState.name) !== -1) ? toState.name : 'login'
        $rootScope.stateChangeBypass = nextStateName !== $state.current.name
        $state.go(nextStateName, toParams)
      }
    }, () => {
          event.preventDefault()
      // if toState is in the list of guestStates, go to that state
      // otherwise redirect to login page
      let nextStateName = (guestStates.indexOf(toState.name) !== -1) ? toState.name : 'login'
      console.log(nextStateName)
      $rootScope.stateChangeBypass = nextStateName !== $state.current.name
      $state.go(nextStateName, toParams)
    })
  })
}
