LoginController.$inject = [
  '$state',
  'User',
  'Auth',
  'toaster',
]

export default function LoginController($state, User, Auth, toaster) {
  const vm = this
  vm.user = {};
  vm.loginError = null

  vm.login = () => {
    User.login(vm.user, data => {
      Auth.setToken(data.results.token)
      Auth.setAuthType(data.results.user_type)
      Auth.setEmail(data.results.email)
      Auth.setName(data.results.first_name + ' ' + data.results.last_name)
      Auth.setGeneralSettingId(null)
      if(data.results.general_setting != null){
        Auth.setGeneralSettingId(data.results.general_setting.id)
        Auth.setCurrentCurrency(data.results.general_setting.currency)
        Auth.setStoreName(data.results.general_setting.store_name)
        Auth.setStoreLogoURL(data.results.general_setting.store_logo.url)
        Auth.setWeightUnit(data.results.general_setting.weight_unit)
        Auth.setCurrentTimeZone(data.results.general_setting.timezone)
      }
      console.log(data.results.general_setting)
      toaster.pop('success', "Login", "Successfully Login")
      $state.go('home')                    // Redirect to students page
    }, data => {
      //vm.loginError = data.data.error
      //console.log(data)
      toaster.pop('error', "Login", data.data.errors.message)
    })
  }
}
