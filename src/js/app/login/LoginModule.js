import LoginController from './controllers/LoginController'

angular
  .module('login', [])
  .controller('LoginController', LoginController)
