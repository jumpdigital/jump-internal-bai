ChangePasswordController.$inject = [
  '$stateParams',
  '$state',
  'User',
  'toaster',
];

export default function ChangePasswordController($stateParams, $state, User, toaster) {
  const vm = this;

  vm.token = $stateParams.token;
  vm.user = null;
  vm.errors = null;
  vm.error = null;

  vm.edit = () => {
    //
  }

  vm.update = (user) => {
    user.reset_password_token = vm.token
    User.reset(user,data => {
        toaster.pop('success', "Login", "Success fully changed password")
        // success
        $state.go('login');
      },data => {
        console.log(data)
        toaster.pop('error', "Login", data.data.errors.message)
        //vm.errors = error.data.errors
        //vm.error = error.data.error
      }
    )
  }
}
