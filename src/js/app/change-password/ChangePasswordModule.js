import ChangePasswordController from './controllers/ChangePasswordController'

angular
  .module('change-password', [])
  .controller('ChangePasswordController', ChangePasswordController)
