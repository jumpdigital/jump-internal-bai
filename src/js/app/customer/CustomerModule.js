import CustomerController from './controllers/CustomerController'
import CustomerModal from './controllers/CustomerModal'
import ImportCustomerModal from './controllers/ImportCustomerModal'
import ExportCustomerModal from './controllers/ExportCustomerModal'
angular
  .module('customers', [])
  .controller('CustomerController', CustomerController)
  .controller('CustomerModal', CustomerModal)
  .controller('ImportCustomerModal', ImportCustomerModal)
  .controller('ExportCustomerModal', ExportCustomerModal)
