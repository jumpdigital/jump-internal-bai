CustomerModal.$inject = ['Customer','$stateParams','$state', '$uibModalInstance', 'modalCustomer'] 
export default function CustomerModal(Customer,$stateParams,$state, $uibModalInstance, modalCustomer) {
  	const vm = this
  	vm.modalCustomer = modalCustomer
	vm.cancel = function () {
    $uibModalInstance.dismiss();
  };
  vm.create = () => {
     Customer.create(vm.modalCustomer,data =>{
     	vm.modalCustomer.newCustomer = data.results
     	$uibModalInstance.dismiss()
     })
   }
}

  