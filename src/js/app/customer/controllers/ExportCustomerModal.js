ExportCustomerModal.$inject = ['Customer','$stateParams','$state', '$uibModalInstance'] 
export default function ExportCustomerModal(Customer,$stateParams,$state, $uibModalInstance) {
  	const vm = this
  	vm.overwrite = false
  	vm.data = null
  	vm.btnCallback = false
  	vm.title = 'Export customers by CSV file'
  	vm.module = 'customers'
    vm.date = new Date()
	vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};

  	vm.getExportData = () => {
    Customer.export_customer(data => {
      vm.ExportData = data.results
    })
  }
}