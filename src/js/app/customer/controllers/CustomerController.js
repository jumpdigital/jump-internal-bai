CustomerController.$inject = ['Customer','StaticData','$stateParams', '$state','$uibModal']

export default function CustomerController(Customer,StaticData, $stateParams, $state, $uibModal) {
  const vm = this

  vm.name = "Hey";
  vm.cnt = 0
  vm.message = "Yehey!"
  vm.errors = null
  vm.checkboxValue = null
  vm.current_address = null
  vm.current_city = null
  vm.current_country = null
  vm.current_zip = null
  vm.currentPage = 1
  vm.BillingPlace = null;
  vm.ShippingPlace = null;
  vm.customer = {}
  vm.country = {}
  vm.country.selected = null
  vm.reverse = true
  // StaticData.setCountries();

  vm.texthead = { name: 'Naomi', address: '1600 Amphitheatre' }
  
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
  };
  vm.all = () => {
    Customer.all(data => {
      vm.customers = data.results
    }

    );

  }

  vm.show = () => {
    if (vm.cnt == 1) {
       vm.message = "Yehey!"
    } else {

       vm.message = null
    }
    Customer.getCustomer({
      id: $stateParams.id
    }, data => { vm.customer = data.results });

  }

  vm.edit = () => {
    Customer.getCustomer({
      id: $stateParams.id
    }, data => {
      vm.customer = data.results
      vm.ShippingPlace = {name: vm.customer.customer_shipping_address.country};
      vm.BillingPlace = {name: vm.customer.customer_billing_address.country};
      console.log(vm.customer)
     });

  }

  vm.update = () => {
     Customer.update(vm.customer,
      data =>{
       console.log(data)
        $state.go('customers.show', {id: vm.customer.id});
     },
     error => {
      vm.errors = error.data.errors
      console.log(vm.errors.length)
     })
   }


  vm.destroy = (id) => {
    vm.customer=
    Customer.destroy({
      id: id
    });
    vm.all()
  }

 vm.create = () => {
    vm.cnt = 1
     Customer.create(vm.customer,
      data =>{
        console.log('sadsad')
        $state.go('customers.show', {id: data.results.id});
     },
     error => {
      vm.errors = error.data.errors
      console.log(vm.errors.length)
     })
   }

  vm.countrylist = () => {
    vm.country = [
      {
        id: 1,
        name: "Philippines"
      },

      {
        id: 2,
        name: "China"

      }
    ], data => { vm.country = vm.country };

  }

  vm.checkVal = () => {
    vm.checkboxValue = vm.checkboxVal;

    if (vm.checkboxValue == "YES") {
      vm.customer.shipping_address = vm.customer.billing_address
      vm.customer.shipping_country = vm.customer.billing_country
      vm.customer.shipping_city = vm.customer.billing_city
      vm.customer.shipping_zip = vm.customer.billing_zip
      vm.ShippingPlace = vm.BillingPlace
      // vm.SelectShippingAddress()

    } else {
      vm.customer.shipping_address = null
      vm.customer.shipping_country = null
      vm.customer.shipping_city = null
      vm.customer.shipping_zip = null
      vm.ShippingPlace = null
    }

  }
  vm.checkValEdit = () => {
    vm.checkboxValue = vm.checkboxVal;

    if (vm.checkboxValue == "YES") {
      vm.customer.customer_shipping_address.street_address = vm.customer.customer_billing_address.street_address
      vm.customer.customer_shipping_address.country = vm.customer.customer_billing_address.country
      vm.customer.customer_shipping_address.city = vm.customer.customer_billing_address.city
      vm.customer.customer_shipping_address.zip_code = vm.customer.customer_billing_address.zip_code
      vm.ShippingPlace = vm.BillingPlace

    } else {
      vm.customer.customer_shipping_address.street_address = vm.current_address
      vm.customer.customer_shipping_address.country = vm.current_country
      vm.customer.customer_shipping_address.city = vm.current_city
      vm.customer.customer_shipping_address.zip_code = vm.current_zip

    }

  }

  vm.deactivateAccount = () => {
    deactivatePrompt()
    vm.customer.deactivate = true
    Customer.deactivate(vm.customer,
      data=>{
        vm.customer = data.results
      }
    )

  }

  let deactivatePrompt = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_deactivate.jade'),
      controller: 'ImportCustomerModal',
      controllerAs: 'vm',
      size: 'lg',
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.index()
      }
    )
  }

  vm.activateAccount = () => {
    activatePrompt()
    vm.customer.deactivate = null
    Customer.deactivate(vm.customer,
      data=>{
        vm.customer = data.results
      }
    )
  }

  let activatePrompt = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_activate.jade'),
      controller: 'ImportCustomerModal',
      controllerAs: 'vm',
      size: 'lg',
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.index()
      }
    )
  }


  vm.SelectBillingAddress = () => {
    vm.checkVal();
    if(vm.BillingPlace !=null && vm.BillingPlace.address_components.length > 0) {
      for (var i=0; i<vm.BillingPlace.address_components.length; i++) {
        if(vm.BillingPlace.address_components[i].types[0] == "locality") {
          vm.customer.billing_city = vm.BillingPlace.address_components[i].long_name;
        }
        if(vm.BillingPlace.address_components[i].types[0] == "country") {
          vm.customer.billing_country = vm.BillingPlace.address_components[i].long_name;
        }
       if(vm.BillingPlace.address_components[i].types[0] == "postal_code") {
          vm.customer.billing_zip = vm.BillingPlace.address_components[i].long_name;
        }
      }
    }
  }

  vm.SelectShippingAddress = () => {
    if(vm.ShippingPlace != null && vm.ShippingPlace.address_components.length > 0) {
      for (var i=0; i<vm.ShippingPlace.address_components.length; i++) {
        if(vm.ShippingPlace.address_components[i].types[0] == "locality") {
          vm.customer.shipping_city = vm.ShippingPlace.address_components[i].long_name;
        }
        if(vm.ShippingPlace.address_components[i].types[0] == "country") {
          vm.customer.shipping_country = vm.ShippingPlace.address_components[i].long_name;
        }
       if(vm.BillingPlace.address_components[i].types[0] == "postal_code") {
          vm.customer.shipping_zip = vm.ShippingPlace.address_components[i].long_name;
        }
      }
    }
  }
  vm.countries = StaticData.getCountries()
  vm.SelectCountry  = () => {
    vm.customer.billing_coutry = vm.country.selected.name
    vm.customer.shipping_coutry = vm.country.selected.name
  }

  vm.openImportModal = () => {
    openImportCustomerModalForm()
  }

  let openImportCustomerModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_import_modal.jade'),
      controller: 'ImportCustomerModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCustomer: function () {
          return vm.modalCustomer
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.all()
      }
    )
  }

  vm.openExportModal = () => {
    openExportCustomerModalForm()
  }

  let openExportCustomerModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_export_modal.jade'),
      controller: 'ExportCustomerModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCustomer: function () {
          return vm.modalCustomer
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.all()
      }
    )
  }
}
