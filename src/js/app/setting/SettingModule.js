import GeneralSettingController from './controllers/GeneralSettingController'
import ShippingSettingController from './controllers/ShippingSettingController'
import ShippingPriceBasedModal from './controllers/ShippingPriceBasedModal'
import ShippingWeightBasedModal from './controllers/ShippingWeightBasedModal'
import TaxSettingController from './controllers/TaxSettingController'
angular
  .module('setting', [])
  .controller('GeneralSettingController', GeneralSettingController)
  .controller('ShippingSettingController', ShippingSettingController)
  .controller('ShippingPriceBasedModal',ShippingPriceBasedModal)
  .controller('ShippingWeightBasedModal',ShippingWeightBasedModal)
  .controller('TaxSettingController', TaxSettingController)
  .filter('num', function() {
    return function(input) {
      return parseFloat(input, 20);
    }
  })