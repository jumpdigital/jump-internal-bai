GeneralSettingController.$inject = [
  '$anchorScroll',
  '$scope',
  '$timeout',
  'GeneralSetting',
  'Auth',
  'TimeZones',
  'Currency',
  'UnitSystem',
  '$resource',
  '$state',
  '$stateParams',
  'Role',
  '$uibModal',
  '$filter',
];

export default function GeneralSettingController($anchorScroll, $scope, $timeout,GeneralSetting,Auth,TimeZones,Currency,UnitSystem,$resource, $state, $stateParams,$uibModal,$filter) {
	const vm = this
	vm.unit_system = {}
	vm.general_setting = {}
	TimeZones.setTimezones();
	UnitSystem.setUnitSystems();
	vm.timezones = TimeZones.getTimezone()
	vm.currencies = Currency.getCurrency()
	vm.unit_systems = UnitSystem.getUnitSystems()
	vm.general_setting_form = {}
	vm.general_setting.account_email = vm.general_setting.account_email ? vm.general_setting.account_email : Auth.getEmail()
	vm.getGeneralSetting = () => {
		// if(!angular.isUndefined(Auth.getGeneralSettingId())){
			GeneralSetting.show({id: 0}, data =>{
			 	Auth.setGeneralSettingId(data.results.id)
				vm.general_setting = data.results
				vm.timezone = vm.timezones.filter((timezone) => timezone.text === vm.general_setting.timezone)[0]
				vm.currency = vm.currencies.filter((currency) => currency.code === vm.general_setting.currency)[0]
				vm.unit_sytem = vm.unit_systems.filter((unit_system) => unit_system.key == vm.general_setting.unit_system)[0]
				vm.weight_unit = vm.unit_sytem.value.filter((symbol) => symbol.symbol === vm.general_setting.weight_unit)[0]
				Auth.setWeightUnit(vm.unit_sytem.value.filter((symbol) => symbol.symbol === vm.general_setting.weight_unit)[0].display)
				vm.general_setting.account_email = vm.general_setting.account_email ? vm.general_setting.account_email : Auth.getEmail();
				vm.general_setting_form.$setPristine()
			},error =>{
		      Auth.deleteGeneralSettingId()
		    })
	}
	
	vm.setGeneralSetting = () => {
		vm.error = null
		vm.isSuccess = false
		const fd = new FormData()
		    for (const key in vm.general_setting) {
	    		if(!angular.isUndefined(vm.general_setting[key]) || vm.general_setting[key] != null){
		     	 	fd.append(key, vm.general_setting[key])
		     	}
		    }
		const address = {"city" : vm.general_setting.city,"zip_code" : vm.general_setting.zip_code, 
						"country": vm.general_setting.country,"street": vm.general_setting.street,
						"phone" : vm.general_setting.phone,"apt_suite" : vm.general_setting.apt_suite}
		    for (const key_address in address) {
		    	console.log(key_address + address.key_address)
	    		if(!angular.isUndefined(address[key_address]) || address[key_address] != null){
		     	 	fd.append("shipping_setting_attributes["+key_address+"]", address[key_address])
		     	}
		    }
		if(angular.isUndefined(Auth.getGeneralSettingId())){
		    return GeneralSetting.create(fd,data =>{
		    	Auth.setGeneralSettingId(data.results.id)
		    	Auth.setCurrentCurrency(data.results.currency)	
		    	 vm.isSuccess = true;
		    	$timeout(function() {
				    vm.isSuccess = false;
				}, 10000); 
				$anchorScroll()
		        $state.go('setting', {}, { reload: true });
		    },error =>{
		      vm.error = error.data.errors
		    })
		}else{
			return GeneralSetting.update({id: Auth.getGeneralSettingId()},fd,data =>{
				vm.currency = vm.currencies.filter((currency) => currency.code === data.results.currency)[0]
				Auth.setCurrentCurrency(data.results.currency)
				vm.isSuccess = true;
				vm.general_setting_form.$setPristine()
				$timeout(function() {
				    vm.isSuccess = false;
				}, 10000);
		        // $location.hash('main-content')
		      	$anchorScroll()
		        $state.go('setting', {}, { reload: true })
		    },error =>{
		      vm.error = error.data.errors
		      // $location.hash('main-content')
		      $anchorScroll()
		    })
		}
	}
}