ShippingWeightBasedModal.$inject = ['UnitSystem','$stateParams','$state', '$uibModalInstance', 'modalWeightbased'] 
export default function ShippingWeightBasedModal(UnitSystem,$stateParams,$state, $uibModalInstance, modalWeightbased) {
  const vm = this
  vm.weight_unit = UnitSystem.getCurrentUnitsystem()
  if (modalWeightbased != null) {
      modalWeightbased.minimum ? modalWeightbased.minimum = parseFloat(modalWeightbased.minimum) : modalWeightbased.minimum 
      modalWeightbased.maximum ? modalWeightbased.maximum = parseFloat(modalWeightbased.maximum) : modalWeightbased.maximum 
      modalWeightbased.amount ? modalWeightbased.amount = parseFloat(modalWeightbased.amount) : modalWeightbased.amount
    }
  vm.modalWeightbased = modalWeightbased
  vm.cancel = function () {
     $uibModalInstance.dismiss()
  }

  vm.add = (data) => {
     vm.modalWeightbased.is_free ? vm.modalWeightbased.is_free = true :  vm.modalWeightbased.is_free = false
	  data.based_rate_type  = 2
    $uibModalInstance.close(data)
  }
}