TaxsettiNgcontrOller.$inject = [
  'Auth',
  'Shipping',
  '$scope',
  '$timeout',
  '$state',
  '$stateParams',
  '$uibModal',
  '$filter',
];

export default function TaxsettiNgcontrOller(Auth, Shipping, $scope, $timeout, $state, $stateParams, $uibModal, $filter) {
	const vm = this
	// StaticData.setCountries()
	vm.stateParams = $stateParams
	vm.zones = []
	vm.taxes = []
	vm.tax_setting = {}
	vm.tax_setting_form = {}
	vm.index = () => {
		vm.taxes.length = 0
		Shipping.index_shiping({id : Auth.getGeneralSettingId()}, data => {
			vm.shipping_setting = data.results
			Shipping.index({id : data.results.id}, data2 => {
				vm.zones = data2.results
				angular.forEach(data2.results, function(result){
					angular.forEach(result.shipping_zone_country, function(shipping_zone_country){
						vm.taxes.push(shipping_zone_country)
					})
					
				})
			})
		})
	}

	vm.show = () => {
		Shipping.show_tax({id : vm.stateParams.id},data =>{
			vm.shipping = data.results
			vm.tax_setting = data.results
			console.log(vm.tax_setting)
			vm.tax_setting_form.$setPristine()
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
	}

	vm.create = function(){
		// vm.shipping.shipping_zone_country_attributes.length = 0
		// angular.forEach(vm.countries.selected, function(country) {
		// 	vm.shipping.shipping_zone_country_attributes.push({"country": country.name})
		// })
		vm.shipping.shipping_setting_id = vm.stateParams.shipping_setting_id
	    Shipping.create(vm.shipping,data =>{
	      $state.go('setting.taxes.edit',{id : data.results.id, CreateShippingZone : data.results.id},{reload : true});
	      vm.shipping = data.results
		  vm.shipping.shipping_zone_based_rate_attributes = data.results.shipping_zone_based_rate
		  vm.shipping.shipping_zone_country_attributes = data.results.shipping_zone_country_attributes
		  	angular.forEach(vm.shipping.shipping_zone_country_attributes, function(country) {
				vm.countries.selected.push({"name" : country.country})
			})
	      vm.btnSubmit = false
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
  	}

  	vm.update = function(){
		vm.shipping.shipping_setting_id = vm.stateParams.id
	    Shipping.update_tax(vm.shipping,data =>{
	      $state.go('setting.taxes.edit',{id : data.results.id, CreateShippingZone : data.results.id},{reload : true});
	      vm.tax_setting = data.results
	      vm.btnSubmit = false
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
  	}
}