ShippingPriceBasedModal.$inject = ['Currency','$stateParams','$state', '$uibModalInstance', 'modalPricebased'] 
export default function ShippingPriceBasedModal(Currency, $stateParams,$state, $uibModalInstance, modalPricebased) {
  	const vm = this
    vm.currency = Currency.getCurrentCurrency()
    if (modalPricebased != null) {
      modalPricebased.minimum ? modalPricebased.minimum = parseFloat(modalPricebased.minimum) : modalPricebased.minimum 
      modalPricebased.maximum ? modalPricebased.maximum = parseFloat(modalPricebased.maximum) : modalPricebased.maximum 
      modalPricebased.amount ? modalPricebased.amount = parseFloat(modalPricebased.amount) : modalPricebased.amount
    }
  	vm.modalPricebased = modalPricebased
  	vm.cancel = function () {
       $uibModalInstance.dismiss()
    };
    vm.add = (data) => {
    	vm.modalPricebased.is_free ? vm.modalPricebased.is_free = true : vm.modalPricebased.is_free = false
    	data.based_rate_type  = 1
        $uibModalInstance.close(data)
    }
}