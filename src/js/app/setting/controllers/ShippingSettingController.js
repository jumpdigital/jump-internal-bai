ShippingSettingController.$inject = [
  'Auth',
  'Shipping',
  '$scope',
  '$timeout',
  'StaticData',
  'UnitSystem',
  '$state',
  '$stateParams',
  '$uibModal',
  '$filter',
];

export default function ShippingSettingController(Auth, Shipping, $scope, $timeout,StaticData, UnitSystem , $state, $stateParams, $uibModal, $filter) {
	const vm = this
	// StaticData.setCountries()
	vm.countries = StaticData.getCountries()
	vm.weight_unit = UnitSystem.getCurrentUnitsystem()
	vm.stateParams = $stateParams
	vm.shipping = {}
	vm.shipping.price_based = []
	vm.shipping.weight_based = []
	vm.shipping.shipping_zone_country_attributes = []
	vm.modalPricebased = null
	vm.modalWeightbased = null
	vm.shipping_setting_form = {}
	vm.shipping.shipping_zone_based_rate_attributes = []
	vm.countries.selected = []
	vm.zones = []
	vm.shipping.general_setting_id = Auth.getGeneralSettingId()
	vm.index = () => {
		Shipping.index_shiping({id : vm.shipping.general_setting_id}, data => {
			vm.shipping_setting = data.results
			Shipping.index({id : data.results.id}, data2 => {
				vm.zones = data2.results
				console.log(vm.zones)
			})
		})
	}

	vm.show = () => {
		Shipping.show({id : vm.stateParams.id},data =>{
			vm.shipping = data.results
			vm.shipping.shipping_zone_based_rate_attributes = data.results.shipping_zone_based_rate
			vm.shipping.shipping_zone_country_attributes = data.results.shipping_zone_country
		  	angular.forEach(vm.shipping.shipping_zone_country_attributes, function(country) {
				vm.countries.selected.push({"name" : country.country})
			})
	      	vm.btnSubmit = false
	      	Shipping.index({id : data.results.shipping_setting_id}, data2 => {
				vm.zones = data2.results
	      		vm.existingCountry()
			})
			vm.shipping_setting_form.$setPristine()
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
	}

	vm.create = function(){
		// vm.shipping.shipping_zone_country_attributes.length = 0
		// angular.forEach(vm.countries.selected, function(country) {
		// 	vm.shipping.shipping_zone_country_attributes.push({"country": country.name})
		// })
		vm.shipping.shipping_setting_id = vm.stateParams.shipping_setting_id
	    Shipping.create(vm.shipping,data =>{
	      $state.go('setting.shipping.edit',{id : data.results.id, CreateShippingZone : data.results.id},{reload : true});
	      vm.shipping = data.results
		  vm.shipping.shipping_zone_based_rate_attributes = data.results.shipping_zone_based_rate
		  vm.shipping.shipping_zone_country_attributes = data.results.shipping_zone_country_attributes
		  	angular.forEach(vm.shipping.shipping_zone_country_attributes, function(country) {
				vm.countries.selected.push({"name" : country.country})
			})
	      vm.btnSubmit = false
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
  	}

  	vm.update = function(){
		// angular.forEach(vm.countries.selected, function(country) {
		// 	vm.shipping.shipping_zone_country_attributes.push({"country": country.name})
		// })
		vm.shipping.shipping_setting_id = vm.stateParams.id
	    Shipping.update(vm.shipping,data =>{
	      $state.go('setting.shipping.edit',{id : data.results.id, CreateShippingZone : data.results.id},{reload : true});
	      vm.shipping = data.results
		  vm.shipping.shipping_zone_based_rate_attributes = data.results.shipping_zone_based_rate
		  vm.shipping.shipping_zone_country_attributes = data.results.shipping_zone_country_attributes
		  	angular.forEach(vm.shipping.shipping_zone_country_attributes, function(country) {
				vm.countries.selected.push({"name" : country.country})
			})
	      vm.btnSubmit = false
	    },error =>{
	      vm.error = error.data.errors
	      vm.btnSubmit = false
	    })
  	}
  	vm.addCountryToList = (country) => {
  		const found = $filter('filter')(vm.shipping.shipping_zone_country_attributes, {country: country.name}, true)[0];
  		if (found){
  			vm.shipping.shipping_zone_country_attributes[vm.shipping.shipping_zone_country_attributes.indexOf(found)] = {"country": country.name, "id": found.id,"_destroy":true}
  		}else{
  			vm.shipping.shipping_zone_country_attributes.push({"country": country.name})
  		}
  	}
  	vm.onRemoveCountryToList = (country) => {
  		const found = $filter('filter')(vm.shipping.shipping_zone_country_attributes, {country: country.name}, true)[0];
  		if (found){
  			vm.shipping.shipping_zone_country_attributes[vm.shipping.shipping_zone_country_attributes.indexOf(found)] = {"country": country.name, "id": found.id,"_destroy":true}
  		}
  		console.log(vm.shipping.shipping_zone_country_attributes)
  	}

  	vm.checkExistingCountry = () => {
  		console.log("yahoo")
  		Shipping.index_shiping({id : vm.shipping.general_setting_id}, data => {
			vm.shipping_setting = data.results
			Shipping.index({id : data.results.id}, data2 => {
				vm.zones = data2.results
				vm.existingCountry()
			})
		})
  	}

  	vm.existingCountry = () => {
  		StaticData.setCountries()
  		angular.forEach(vm.zones, function(shippings) {
  			// const exist_shipping = shippings.shipping_zone_country.filter((shipping_zone) => shipping_zone.country === country)[0]
  			// if(exist_shipping){
				// vm.countries.selected.indexOf({"name":})
			// }
			angular.forEach(shippings.shipping_zone_country, function(zone_country){
				// const data = {"name": 'Afghanistan'}
				// const text = vm.countries.indexOf(data)
				const exist_country = vm.countries.map(function(e) { return e.name; }).indexOf(zone_country.country)
				const exist_selected_country = vm.countries.selected.map(function(e) { return e.name; }).indexOf(zone_country.country)
				if(exist_country > -1 && exist_selected_country == -1){
					vm.countries[exist_country].isUsed = true
					console.log(vm.countries[exist_country].name)
				}
			})
		})
  	}

	vm.openPriceBasedModalForm = (index) => {
	    let modalInstance = $uibModal.open({
	      template: require('js/app/setting/templates/shipping_price_based_modal.jade'),
	      controller: 'ShippingPriceBasedModal',
	      controllerAs: 'vm',
	      size: 'lg',
	      resolve: {
	        modalPricebased: function () {
	          angular.isUndefined(index) ? vm.modalPricebased = null : vm.modalPricebased = vm.shipping.shipping_zone_based_rate_attributes[index]
	          return vm.modalPricebased
	        }
	      }
	    })

	    modalInstance.result.then(
	      function (result) {
	      	angular.isUndefined(index) ? vm.shipping.shipping_zone_based_rate_attributes.push(result) : vm.shipping.shipping_zone_based_rate_attributes[index] = result
	      	vm.shipping_setting_form.modified = true
	      	console.log(vm.shipping.shipping_zone_based_rate_attributes)
	      },
	      function (data) {
	      }
	    )
  	}

  	vm.openWeightBasedModalForm = (index) => {
	    let modalInstance = $uibModal.open({
	      template: require('js/app/setting/templates/shipping_weight_based_modal.jade'),
	      controller: 'ShippingWeightBasedModal',
	      controllerAs: 'vm',
	      size: 'lg',
	      resolve: {
	        modalWeightbased: function () {
	          angular.isUndefined(index) ? vm.modalWeightbased = null : vm.modalWeightbased = vm.shipping.shipping_zone_based_rate_attributes[index]
	          return vm.modalWeightbased
	        }
	      }
	    })

	    modalInstance.result.then(
	      function (result) {
	      	angular.isUndefined(index) ? vm.shipping.shipping_zone_based_rate_attributes.push(result) : vm.shipping.shipping_zone_based_rate_attributes[index] = result
	      	vm.shipping_setting_form.modified = true
	      },
	      function (data) {
	      }
	    )
  	}
}