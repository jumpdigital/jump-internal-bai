import ProductCategoryController from './controllers/ProductCategoryController'

angular
  .module('product_category', [])
  .controller('ProductCategoryController', ProductCategoryController)
