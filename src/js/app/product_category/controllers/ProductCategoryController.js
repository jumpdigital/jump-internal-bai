ProductCategoryController.$inject = ['ProductCategory','$stateParams','$state']
export default function ProductCategoryController(ProductCategory,$stateParams,$state) {
  const vm = this
  vm.action = {}
    vm.productCategoryObject = {}
  vm.error = null
  vm.currentPage = 1
  vm.actions = [
        {'name': 'Action'},
        {'name': 'Delete'}
    ];
  vm.reverse = true
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
  };
  vm.getCategories = function(){

    ProductCategory.categories(data => {
      vm.categories = data.results;
      console.log(data)
    })
  }
  vm.set_url = (name) => {
    if (name !== undefined) {
      var temp_url = "/" + name
      vm.url = temp_url.replace(/ /g,"_");
    } else {
      vm.url = "";
    }

  }

  vm.createCategory = function(category){
    category.url = vm.url
    const fd = new FormData()
    for (const key in category) {
      fd.append(key, category[key]);
    }
    ProductCategory.createCategory(fd,data =>{
         $state.go('category_product', {}, { reload: true });
        $state.go('category_product.show', {id: data.results.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }
  vm.getCategory = function() {

    ProductCategory.show({id: $stateParams.id },data => {
      vm.category = data.results
      vm.url = vm.category.url
    })
  }
  vm.getProductCategory = function(){

    ProductCategory.product_categories(data => {
      vm.categories = data.results
    })
  }

  vm.updateCategory = function(){
    vm.category.url = vm.url
    const fd = new FormData()
    for (const key in vm.category) {
      fd.append(key, vm.category[key]);
    }
    ProductCategory.update({id: vm.category.id},fd,data =>{
       $state.go('category_product.show', {id: $stateParams.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.deleteCategory = function(category){
    ProductCategory.delete({id: category.id},data=> {
      vm.categories.splice(vm.categories.indexOf(category), 1 );
    })
  }


  vm.selectAll = function() {
      angular.forEach(vm.categories, function(category) {
        category.Selected = vm.selectedAll;
      });
    };

  vm.checkIfAllSelected = function() {
      vm.selectedAll = vm.categories.every(function(category) {
        return category.Selected == true
      })
    };

  vm.delete = function(){
    if(vm.action == 'Delete'){
      vm.productCategoryObject.categories = {}
      vm.productCategoryObject.categories.id = []
      angular.forEach(vm.categories, function(category) {
        if(category.Selected == true){
          vm.productCategoryObject.categories.id.push(category.id)
        }
      })
      ProductCategory.multipleDelete(vm.productCategoryObject ,data=> {
        vm.getCategories()
      })
    }
  }
}
