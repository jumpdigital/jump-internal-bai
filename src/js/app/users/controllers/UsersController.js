UsersController.$inject = [
  'User',
  '$resource',
  '$state',
  '$stateParams',
  'Role',
  '$uibModal'
];

export default function UsersController(User, $resource, $state, $stateParams, Role,$uibModal) {
  const vm = this;
  vm.errors = null

  // resources
  // to make these as factory/service later when necessary.

  // sadly, $resource has no update() at its core
  // const userActions = {
  //   'update': {
  //     url: '/api/users/:id',
  //     params: {
  //       id: '@id'
  //     },
  //     method: 'PATCH'
  //   }
  // }

  // var User = $resource('/api/users/:id', null, userActions);
  // var Role = $resource('/api/users/roles');

  // explicitly initialize all vars
  vm.users = null;
  vm.user = null;
  vm.roles = null;
  vm.errors = null;
  vm.currentPage = 1
   vm.reverse = true
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
  };
  vm.index = () => {
    User.list(data => {
      vm.users = data.results
    })
  }

  vm.new = () => {
    Role.all(data => {
     vm.roles = data.results
    });
    console.log(vm.roles)
  }

  vm.create = (user) => {
     User.create(user,
      data =>{
       console.log(data)
        $state.go('users.show', {id: data.results.id});
     },
     error => {
      vm.errors = error.data.errors
      console.log(vm.errors.length)
     })
   }

  vm.show = () => {
    User.getUser({
      id: $stateParams.id
    },
    data => { vm.user = data.results });
  }

  vm.edit = () => {
    Role.all(data => {
     vm.roles = data.results
    });

    User.getUser({
      id: $stateParams.id
    },
    data => { vm.user = data.results });
  }

  vm.update = (user) => {
     User.update(user,data =>{
        $state.go('users.show', {id: $stateParams.id});
     },
     error => {
      vm.errors = error.data.errors
      console.log(vm.errors.length)
     })
   }

  vm.delete = (user) => {
    vm.users=
    User.destroy({
      id: user.id
    });
    vm.index()
  }

  let getRoles = () => {
    Role.all(data => {
      return data.results
    });
  }

  vm.cancel = function () {
      $uibModalInstance.dismiss();
  };

  vm.deactivateAccount = () => {
    deactivatePrompt()
    vm.user.deactivate = true
    User.deactivate(vm.user,
      data=>{
        vm.user = data.results
      }
    )

  }

  let deactivatePrompt = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_deactivate.jade'),
      controller: 'ExportUserModal',
      controllerAs: 'vm',
      size: 'lg',
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.index()
      }
    )
  }

  vm.activateAccount = () => {
    activatePrompt()
    vm.user.deactivate = null
    User.deactivate(vm.user,
      data=>{
        vm.user = data.results
      }
    )

  }

  let activatePrompt = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_activate.jade'),
      controller: 'ExportUserModal',
      controllerAs: 'vm',
      size: 'lg',
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.index()
      }
    )
  }

  vm.openImportModal = () => {
    openImportModalForm()
  }

  let openImportModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_import_modal.jade'),
      controller: 'ImportUserModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCustomer: function () {
          return vm.modalCustomer
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.index()
      }
    )
  }

  vm.openExportModal = () => {
    openExportModalForm()
  }

  let openExportModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_export_modal.jade'),
      controller: 'ExportUserModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalUser: function () {
          return vm.modalUser
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
      }
    )
  }

}
