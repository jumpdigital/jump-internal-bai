ImportUserModal.$inject = ['User','$stateParams','$state', '$uibModalInstance',] 
export default function ImportUserModal(User,$stateParams,$state, $uibModalInstance) {
  	const vm = this
  	vm.overwrite = false
  	vm.data = null
  	vm.btnCallback = false
  	vm.title = 'Import user by CSV file'
	vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};

  	vm.import = function(file){
	    const fd = new FormData()
	    fd.append("file", file);
	    fd.append("overwrite", vm.overwrite)
	    User.import(fd,data =>{
	      vm.btnCallback = true
	      $uibModalInstance.dismiss();
	    },error =>{
	      vm.data = error.data
	      vm.btnCallback = true
	    })
  	}
}

  