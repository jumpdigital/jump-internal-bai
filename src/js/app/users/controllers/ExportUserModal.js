ExportUserModal.$inject = ['User','$stateParams','$state', '$uibModalInstance'] 
export default function ExportUserModal(User,$stateParams,$state, $uibModalInstance) {
    const vm = this
    vm.overwrite = false
    vm.data = null
    vm.btnCallback = false
    vm.title = 'Export users by CSV file'
    vm.module = 'users'
    vm.date = new Date()
  vm.cancel = function () {
      $uibModalInstance.dismiss();
    };

    vm.getExportData = () => {
    User.export(data => {
      vm.ExportData = data.results
    })
  }
}