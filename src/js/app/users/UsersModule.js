import UsersController from './controllers/UsersController'
import ExportUserModal from './controllers/ExportUserModal'
import ImportUserModal from './controllers/ImportUserModal'

angular
  .module('users', [])
  .controller('UsersController', UsersController)
  .controller('ImportUserModal', ImportUserModal)
  .controller('ExportUserModal', ExportUserModal)
