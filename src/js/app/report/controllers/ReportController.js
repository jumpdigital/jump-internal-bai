ReportController.$inject = [
  'Report',
  'Order',
  'Currency',
  '$resource',
  '$state',
  '$stateParams',
];

export default function ReportController(Report, Order, Currency, $resource, $state, $stateParams) {
  	const vm = this
  	vm.currency = Currency.getCurrentCurrency()
  	vm.errors = null
  	vm.orders = []
  	vm.tmp_data = []
  	vm.tmp_dataCancelled = []
  	vm.tmp_dataInvoiced = []
  	vm.totalSales = 0
  	vm.totalCancelled = 0
  	vm.totalInvoiced = 0
  	vm.totalOrders = 0
    vm.tmp_order_products = []
    vm.tmp_product_quantity = []
    vm.products = []

    vm.addTmpDate = (date) =>{
    	vm.tmp_date.push(date)
    }
    var MMMM = 'MMMM-yyyy'
    var column = 'created_at'
    vm.format =''+column+'| date:"'+MMMM+'"'

    vm.dateFormat = (column, format) =>{

    	return ''+column+'| date:"'+format+'"'
    }

    vm.SalesGraph = () => {
	   vm.data =[vm.tmp_data, vm.tmp_dataCancelled]
	   vm.labels =[]
	  vm.series = ['Sales', 'Cancelled'];
  	}

  	vm.InvoiceGraph = () => {
  		vm.InvoiceData = [vm.tmp_dataInvoiced, vm.tmp_dataCancelled]
  		vm.InvoiceLabels = []
  		vm.InvoiceSeries = ['Invoice','Cancelled']
  	}

	vm.getOrders = function(){
	    Order.order(data => {
	      	vm.orders = data.results
          angular.forEach(vm.orders, function(order){
            angular.forEach(order.order_product, function (product){
              product.order_created_at = order.created_at
              product.order_updated_at = order.updated_at
              vm.products.push(product)
            })
          })
	    })
	}
	vm.getTotalSales = function(orders, isLast){
		var sale_sum = 0
		var order_status = [2,3,4,5]
		angular.forEach(orders, function(order){
			if(order.order_history.order_status) {
				if(order_status.indexOf(order.order_history.order_status.id)!== -1){
					sale_sum += parseFloat(order.grand_total,20)
				}
			}
        })
        return sale_sum
	}

	vm.getTotalInvoice = function(orders){
		var invoice = 0
		angular.forEach(orders, function(order){
			if(order.order_history.order_status) {
				if(order.order_history.order_status.id == 5){
					invoice += parseFloat(order.grand_total,20)
				}
			}

        })
        return invoice
	}

	vm.getCancelledOrders = function(orders){
		var cancelled = 0
		angular.forEach(orders, function(order){
			if(order.order_history.order_status) {
				if(order.order_history.order_status.id == 6){
					cancelled += parseFloat(order.grand_total,20)
				}
			}

        })
        return cancelled
	}

	vm.toArray = (order) => {
    console.log(order)
  }
  vm.tmpOrderProducts = (orders) =>{

    console.log(vm.tmp_order_products)
    vm.tmp_order_products = []
    angular.forEach(orders, function(order){
      angular.forEach(order.order_product, function(product){
        vm.tmp_order_products.push(product)
      })
    })
    // console.log(vm.tmp_order_products)
  }
  vm.tmpProductQuantity = (quantity) => {
    vm.tmp_product_quantity.push(quantity)
  }
}
