import ReportController from './controllers/ReportController'

angular
  .module('report', [])
  .controller('ReportController', ReportController)
  .filter('num', function() {
    return function(input) {
      return parseFloat(input, 20);
    }
  })
