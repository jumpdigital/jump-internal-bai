import SubCategoryController from './controllers/SubCategoryController'

angular
  .module('subcategory', [])
  .controller('SubCategoryController', SubCategoryController)
