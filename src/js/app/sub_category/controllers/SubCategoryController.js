SubCategoryController.$inject = ['SubCategory', '$stateParams', '$state']
export default function SubCategoryController(SubCategory, $stateParams, $state) {
  const vm = this

  vm.name = "Hey";
  vm.cnt = 0
  vm.currentPage = 1
  vm.subcategoryObject = {}
  vm.actions = [
        {'name': 'Action'},
        {'name': 'Delete'}
  ];
   vm.reverse = true
   var subcategory = {}
    vm.sortBy = function(propertyName) {
        vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
        vm.propertyName = propertyName;
    };
  vm.all = () => {
    SubCategory.all(data => {
      vm.sub_categories = data.results
      // console.log(vm.sub_categories)
    }

    );

  }

  vm.set_url = (category_id, name) => {
    var category_name = null
    SubCategory.product_categories(data => {
      vm.categories = data.results
      angular.forEach(vm.categories, function (category) {
        if (category.id == category_id) {
          category_name = category.name
        }
      })

      if (category_name !== undefined && name !== undefined) {
        var temp_url = "/" + category_name + "_" + name
        vm.url = temp_url.replace(/ /g,"_");

      } else {
        vm.url = "";
      }
    })



  }

  vm.getProductCategory = function(){

    SubCategory.product_categories(data => {

      angular.forEach(data.results, function(category) {
          if(category.name.toLowerCase() == 'uncategorized'){
            subcategory.category_id = category.id
          }
      });
      vm.categories = data.results
      console.log(vm.categories)
    })
  }

  vm.createSubCategory = (subcategory) => {
    subcategory.url = vm.url
    const fd = new FormData()
    for (const key in subcategory) {
      fd.append(key, subcategory[key]);
    }
    SubCategory.create(fd,data =>{
        //  $state.go('subcategory', {}, { reload: true });
        $state.go('subcategory.show', {id: data.results.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.showSubCategory = () => {
    SubCategory.show({
      id: $stateParams.id
    }, data => { vm.subcategory = data.results
      vm.url = vm.subcategory.url
      vm.subcategory.category = vm.subcategory.category
      console.log("ana");
      console.log(vm.subcategory.category);
    });
  }


  vm.updateSubCategory = () => {
    vm.subcategory.url = vm.url
    const fd = new FormData()
    for (const key in vm.subcategory) {
      fd.append(key, vm.subcategory[key]);
    }
    SubCategory.update({id: vm.subcategory.id},fd,data =>{
        //  $state.go('subcategory', {}, { reload: true });
        $state.go('subcategory.show', {id: data.results.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.deleteSubCategory = (subcategory) =>{
    console.log(subcategory)
    SubCategory.delete({id: subcategory.id},data=> {
      vm.sub_categories.splice( vm.sub_categories.indexOf(subcategory), 1 );
    },error =>{
      vm.error = error.data.errors
    })
  }


  vm.multipleDelete = () => {
    if(vm.action == 'Delete'){
      vm.subcategoryObject.subcategories = {}
      vm.subcategoryObject.subcategories.id = []
      angular.forEach(vm.sub_categories, function(subcategory) {
        if(subcategory.Selected == true){
          vm.subcategoryObject.subcategories.id.push(subcategory.id)
        }
      })
      SubCategory.multipleDelete(vm.subcategoryObject ,data=> {
        vm.all()
      })
    }
  }

  vm.selectAll = () => {
      angular.forEach(vm.sub_categories, function(subcategory) {
        subcategory.Selected = vm.selectedAll;
      });
    };

  vm.checkIfAllSelected = () => {
      vm.selectedAll = vm.sub_categories.every(function(sub_category) {
        return sub_category.Selected == true
      })
    };

}
