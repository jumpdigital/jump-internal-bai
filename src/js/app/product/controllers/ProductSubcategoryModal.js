ProductSubcategoryModal.$inject = ['Product','$stateParams','$state', '$uibModalInstance','modalSubcategory']
export default function ProductSubcategoryModal(Product,$stateParams,$state, $uibModalInstance, modalSubcategory) {
  	const vm = this

  	vm.modalSubcategory = modalSubcategory

	  vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};
  	vm.createSubcategory = function(subcategory){
	    const fd = new FormData()
	    for (const key in subcategory) {
	      fd.append(key, subcategory[key]);
	    }
	    Product.createSubcategory(fd,data =>{
	    	vm.modalSubcategory = data.results
	      $uibModalInstance.dismiss();
	    },error =>{
	      vm.error = error.data.errors
	    })
  	}

    vm.getProductCategory = function(){

      Product.product_categories(data => {
        vm.categories = data.results
      })
    }
}
