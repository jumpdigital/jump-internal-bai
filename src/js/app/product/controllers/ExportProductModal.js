ExportProductModal.$inject = ['Product','$stateParams','$state', '$uibModalInstance'] 
export default function ExportProductModal(Product,$stateParams,$state, $uibModalInstance) {
  	const vm = this
  	vm.overwrite = false
  	vm.data = null
  	vm.btnCallback = false
  	vm.title = 'Export products by CSV file'
  	vm.module = 'products'
    vm.date = new Date()
	  vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};

  	vm.getExportData = () => {
    console.log("sample")
    Product.export_product(data => {
      vm.ExportData = data.results
    })
  }
}