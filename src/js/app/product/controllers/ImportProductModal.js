ImportProductModal.$inject = ['Product','$stateParams','$state', '$uibModalInstance','modalCategory'] 
export default function ImportProductModal(Product,$stateParams,$state, $uibModalInstance, modalCategory) {
  	const vm = this
  	vm.overwrite = false
  	vm.data = null
  	vm.btnCallback = false
  	vm.title = 'Import products by CSV file'
	vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};

  	vm.import = function(file){
	    const fd = new FormData()
	    fd.append("file", file);
	    fd.append("overwrite", vm.overwrite)
	    Product.import_product(fd,data =>{
	      vm.btnCallback = true
	      $uibModalInstance.dismiss();
	    },error =>{
	      vm.data = error.data
	      vm.btnCallback = true
	    })
  	}
}

  