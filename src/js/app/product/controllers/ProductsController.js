ProductsController.$inject = ['Product','Currency','$stateParams','$state', '$uibModal']
export default function ProductsController(Product,Currency,$stateParams,$state, $uibModal) {
  const vm = this
//  vm.file = {}
  vm.currency = Currency.getCurrentCurrency()
  vm.item = true
  vm.new_product = {}
  vm.new_product.image = null
  vm.productObject = {}
  vm.action = {}
  vm.error = null
  vm.selectedFiles = null
  vm.product = {}
  vm.modalCategory = {}
  vm.modelSubcategory = {}
  vm.actions = [
        {'name': 'Action'},
        {'name': 'Delete'}
    ];
  vm.new_product.in_stock = true
  vm.currentPage = 1;
  vm.reverse = true
  vm.productcat = []
  vm.subcategoryoptions = []
  vm.productsub = []
  vm.productcategories = []
  vm.productsubcategories = []
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
  };
  vm.new_product.category_id = []
  vm.alltags = []
  vm.tag = []
  vm.importProudct = function(file){
    const fd = new FormData()
    fd.append("file", file);
    Product.import_product(fd,data =>{
      // $state.go('product.show', {id: data.results.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.getProducts = function(){

    Product.product(data => {
      vm.products = data.results
    })
  }
  vm.submitForm = function(product) {
    vm.loadedFileName = product.someFile.name;
  };

  vm.tags = (query) => {
    Product.getTags(data => {
      vm.alltags = []
      vm.gettags = data.results
      angular.forEach(vm.gettags, function(t) {
        if (vm.alltags.indexOf(t.tag) == -1) {
          vm.alltags.push(t.tag)
        }

      })
    })
    return vm.alltags
  }

  vm.set_url = (name, sku) => {
    Product.product(data => {
      vm.allproducts = data.results
      vm.name_count = 0
      angular.forEach(vm.allproducts, function(product) {
        if (product.name == name) {
          vm.name_count ++;
        }
      });
      if (vm.name_count == 0 && sku !== undefined && name !== undefined) {
        vm.name_count = ''
        var temp_url = "/" + sku + "-" + name
        vm.url = temp_url.replace(/ /g,"_");
      } else if (vm.name_count > 0 && sku !== undefined && name !== undefined) {
        var temp_url = "/" + sku + "-" + name + "-"+ vm.name_count
        vm.url = temp_url.replace(/ /g,"_");
      } else {
        vm.url = "";
      }

    })


  }

  vm.newProduct = function(product){
    vm.productcat = []
    vm.productsub = []
    vm.productTags = []
    product.url = vm.url
    angular.forEach(vm.new_product.tag, function(t) {
      vm.productTags.push(t.text)
    });



    angular.forEach(vm.new_product.category_id, function(c) {
      vm.productcat.push(c.id)
    });


    angular.forEach(vm.new_product.subcategory_id, function(s) {
      vm.productsub.push(s.id)
    });
    const fd = new FormData()
    for (const key in product) {
      fd.append(key, product[key]);
    }
    for (const key in vm.selectedFiles){
      fd.append('gallery[]',vm.selectedFiles[key])
    }

    for (const key in vm.productcat){
      fd.append('category[]',vm.productcat[key])
    }

    for (const key in vm.productsub){
      fd.append('sub_category[]',vm.productsub[key])
    }

    for (const key in vm.productTags){
      fd.append('tags[]',vm.productTags[key])
    }

    Product.newProduct(fd,data =>{
      $state.go('product.show', {id: data.results.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.subCategoriesOption = (category) => {
    vm.subcategoryoptions = []
    vm.getProductSubCategory();
    if (vm.productcategories == null) {
      vm.subcategoryoptions = []
    } else {
      angular.forEach(category, function(c) {
        angular.forEach(vm.subcategories, function(s) {
          if (s.category_id == c.id) {
            vm.subcategoryoptions.push(s)
          }
        });
      });
    }


  }
  vm.getProduct = () => {
    vm.subcategoryoptions = []
    Product.show({id: $stateParams.id },data => {
      vm.product = data.results
      vm.product.price = parseFloat(vm.product.price)
      vm.product.discounted_price = parseFloat(vm.product.discounted_price)
      vm.url = vm.product.url
      angular.forEach(vm.product.product_categories, function(category) {
          vm.productcategories.push(category.category)
      });
      if (vm.product.product_subcategories.length != 0) {
        angular.forEach(vm.product.product_subcategories, function(subcategory) {
            vm.productsubcategories.push(subcategory.sub_category)
        });
      }

      if ($state.current.name == 'product.edit') {
          angular.forEach(vm.product.product_categories, function(productcategory) {
            productcategory.category.sub_categories = productcategory.category.sub_categories.map(function(e) {
              vm.subcategoryoptions.push(e)
            });
          });

      }

    angular.forEach(vm.product.product_tag,function(tag) {
        vm.addtag={
          text: tag.tag
        }
        vm.tag.push(vm.addtag)
      });

      console.log(vm.tag);
    });

    vm.product_tag = vm.tag


  }
  vm.getProductCategory = function(){

    Product.product_categories(data => {

      angular.forEach(data.results, function(category) {
          if(category.name.toLowerCase() == 'uncategorized'){
            vm.new_product.category_id.push(category)
          }
      });
      vm.categories = data.results
    })
  }

  vm.getProductSubCategory = function(){

    Product.product_subcategories(data => {
      vm.subcategories = data.results
    })
  }

  vm.updateProduct = function(){
    vm.productcat = []
    vm.productsub = []
    vm.productTags = []
    vm.product.url = vm.url
    angular.forEach(vm.product_tag, function(t) {
      vm.productTags.push(t.text)
    });
    angular.forEach(vm.productcategories, function(c) {
      vm.productcat.push(c.id)
    });

    angular.forEach(vm.productsubcategories, function(s) {
      vm.productsub.push(s.id)
    });

    const fd = new FormData()
    for (const key in vm.product) {
      fd.append(key, vm.product[key]);
    }
    for (const key in vm.productcat){
      fd.append('category[]',vm.productcat[key])
    }

    for (const key in vm.productsub){
      fd.append('sub_category[]',vm.productsub[key])
    }

    for (const key in vm.productTags){
      fd.append('tags[]',vm.productTags[key])
    }


    Product.update({id: vm.product.id},fd,data =>{
       $state.go('product.show', {id: $stateParams.id}, { reload: true });
    },error =>{
      vm.error = error.data.errors
    })
  }

  vm.deleteProduct = function(product){
    Product.delete({id: product.id},data=> {
      vm.products.splice( vm.products.indexOf(product), 1 );
    },error =>{
      vm.error = error.data.errors
    })
  }
  vm.multipleDelete = function(){

    vm.productObject.products = {}
    vm.productObject.products.id = []
    vm.products.every(function(product) {
        if(product.Selected == true){
          vm.productObject.product.id.push(product.id)
        }
    })
    Product.multipleDelete(vm.productObject ,data=> {
      vm.products.every(function(product) {
        if(product.Selected == true){
          vm.products.splice(vm.products.indexOf(product), 1 );
        }
      })
    },error =>{
      vm.error = error.data.errors
    })
  }
  vm.selectAll = function() {
      angular.forEach(vm.products, function(product) {
        product.Selected = vm.selectedAll;
      });
    };

  vm.checkIfAllSelected = function() {
      vm.selectedAll = vm.products.every(function(product) {
        return product.Selected == true
      })
    };
  vm.log = function(){
    if(vm.action == 'Delete'){
      vm.productObject.products = {}
      vm.productObject.products.id = []
      angular.forEach(vm.products, function(product) {
        if(product.Selected == true){
          vm.productObject.products.id.push(product.id)
        }
      })
      Product.multipleDelete(vm.productObject ,data=> {
        vm.getProducts()
      },error =>{
      vm.error = error.data.errors
      })
    }
  }

  vm.selectFiles = function(files){
    vm.selectedFiles = files
    vm.product.product_gallery = files
  }

  vm.openCategoryModal = () => {
    openProductCategoryModalForm()
  }
  vm.openSubcategoryModal = () => {
    openProductSubcategoryModalForm()
  }
  let openProductCategoryModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/product/templates/product_category_modal.jade'),
      controller: 'ProductCategoryModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCategory: function () {
          return vm.modalCategory
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
        vm.categories.push(vm.modalCategory.newCategory)
        vm.new_product.category_id = vm.modalCategory.newCategory.id
      }
    )
  }

  let openProductSubcategoryModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/product/templates/sub_category_modal.jade'),
      controller: 'ProductSubcategoryModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalSubcategory: function () {
          return vm.modalSubcategory
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      // function () {
        // vm.subcategories.push(vm.modalSubcategory.newSubcategory)
        // vm.new_product.subcategory_id = vm.modalSubcategory.newSubcategory.id
      // }
    )
  }
  vm.openImportModal = () => {
    openImportProductModalForm()
  }

  let openImportProductModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_import_modal.jade'),
      controller: 'ImportProductModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCategory: function () {
          return vm.modalCategory
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.getProducts()
      }
    )
  }

  vm.openExportModal = () => {
    openExportCustomerModalForm()
  }

  let openExportCustomerModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_export_modal.jade'),
      controller: 'ExportProductModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCategory: function () {
          return vm.modalCategory
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.getProducts()
      }
    )
  }
  vm.getExportData = () => {
    Product.export_product(data => {
      vm.ExportData = data.results
    })
  }
}
