ProductCategoryModal.$inject = ['Product','$stateParams','$state', '$uibModalInstance','modalCategory']
export default function ProductCategoryModal(Product,$stateParams,$state, $uibModalInstance, modalCategory) {
  	const vm = this

  	vm.modalCategory = modalCategory

    vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};
  	vm.createCategory = function(category){
	    const fd = new FormData()
	    for (const key in category) {
	      fd.append(key, category[key]);
	    }
	    Product.createCategory(fd,data =>{
	    	vm.modalCategory.newCategory = data.results
	        $uibModalInstance.dismiss();
	    },error =>{
	      vm.error = error.data.errors
	    })
  	}

}
