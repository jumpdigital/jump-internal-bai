import ProductsController from './controllers/ProductsController'
import ProductCategoryModal from './controllers/ProductCategoryModal'
import ImportProductModal from './controllers/ImportProductModal'
import ExportProductModal from './controllers/ExportProductModal'
import ProductSubcategoryModal from './controllers/ProductSubcategoryModal'

angular
  .module('product', [])
  .controller('ProductsController', ProductsController)
  .controller('ProductCategoryModal', ProductCategoryModal)
  .controller('ImportProductModal', ImportProductModal)
  .controller('ExportProductModal',ExportProductModal)
  .controller('ProductSubcategoryModal', ProductSubcategoryModal)
  .filter('isArray', function() {
	  return function (input) {
	    return angular.isArray(input)
	  }
	})
