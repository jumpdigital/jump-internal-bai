BannerController.$inject = ['Banner', '$stateParams', '$state']
export default function BannerController(Banner, $stateParams, $state) {
  const vm = this

  vm.name = "Hey";
  vm.cnt = 0
  vm.currentPage = 1
  vm.bannerObject = {}
  vm.actions = [
        {'name': 'Action'},
        {'name': 'Delete'}
  ];
   vm.reverse = true
   var subcategory = {}
    vm.sortBy = function(propertyName) {
        vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
        vm.propertyName = propertyName;
    };


   vm.all = () => {
     Banner.all(data => {
       vm.banners = data.results
     });
   }

   vm.createBanner = (banner) => {
     const fd = new FormData()
     for (const key in banner) {
       fd.append(key, banner[key]);
     }
     Banner.create(fd,data =>{
        $state.go('banner.show', {id: data.results.id}, { reload: true });
     },error =>{
       vm.error = error.data.errors
     })
   }

   vm.showBanner = () => {
     Banner.getBanner({id: $stateParams.id },data => {
       vm.banner = data.results
     })
   }

   vm.updateBanner = () => {
     const fd = new FormData()
     for (const key in vm.banner) {
       fd.append(key, vm.banner[key]);
     }

     Banner.update({id: vm.banner.id},fd,data =>{
        $state.go('banner.show', {id: $stateParams.id}, { reload: true });
     },error =>{
       vm.error = error.data.errors
     })
   }

   vm.deleteBanner = (banner) =>{
     Banner.delete({id: banner.id},data=> {
       vm.banners.splice( vm.banners.indexOf(banner), 1 );
     },error =>{
       vm.error = error.data.errors
     })
   }

   vm.multipleDelete = () => {
     if(vm.action == 'Delete'){
       vm.bannerObject.banners = {}
       vm.bannerObject.banners.id = []
       angular.forEach(vm.banners, function(banner) {
         if(banner.Selected == true){
           vm.bannerObject.banners.id.push(banner.id)
         }
       })
       Banner.multipleDelete(vm.bannerObject ,data=> {
         vm.all()
       })
     }
   }

   vm.selectAll = () => {
       angular.forEach(vm.banners, function(banner) {
         banner.Selected = vm.selectedAll;
       });
     };

   vm.checkIfAllSelected = () => {
       vm.selectedAll = vm.banners.every(function(banner) {
         return banner.Selected == true
       })
     };



}
