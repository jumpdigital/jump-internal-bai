import BannerController from './controllers/BannerController'

angular
  .module('banner', [])
  .controller('BannerController', BannerController)
