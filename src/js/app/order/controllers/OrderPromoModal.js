OrderPromoModal.$inject = ['Order','$stateParams','$state', '$uibModalInstance','modalPromo','$filter']
export default function OrderPromoModal(Order,$stateParams,$state, $uibModalInstance, modalPromo,$filter) {
  	const vm = this
	vm.modalPromo = modalPromo
	vm.errorMessage = null
	vm.tmpPromo = vm.modalPromo
	vm.promos = []
	vm.cancel = function () {
   		$uibModalInstance.dismiss()
	}
	vm.getPromos = () => {
		Order.getPromos(data => {
			angular.forEach(data.results, function (promo){
	    		if(!$filter('filter')(vm.tmpPromo, {id: promo.id})[0]){
	               vm.promos.push(promo)
	       		}
    		})
		})
	}
	vm.addPromo = () => {
		vm.modalPromo.length = 0
    	angular.forEach(vm.tmpPromo, function (promo){
    		console.log(promo)
    		if(!$filter('filter')(vm.modalPromo, {id: promo.id})[0]){
               vm.modalPromo.push(promo)
       		}
    	})
    	$uibModalInstance.dismiss()
    }

}
