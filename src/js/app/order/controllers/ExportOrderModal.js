ExportOrderModal.$inject = ['Order','$stateParams','$state', '$uibModalInstance'] 
export default function ExportOrderModal(Order,$stateParams,$state, $uibModalInstance) {
  	const vm = this
  	vm.overwrite = false
  	vm.data = null
  	vm.btnCallback = false
  	vm.title = 'Export orders by CSV file'
  	vm.module = 'orders'
    vm.date = new Date()
	  vm.cancel = function () {
    	$uibModalInstance.dismiss();
  	};

  	vm.getExportData = () => {
    Order.export_orders(data => {
      vm.ExportData = data.results
    })
  }
}