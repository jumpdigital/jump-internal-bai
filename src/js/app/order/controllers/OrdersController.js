OrdersController.$inject = ['Auth','Order','Shipping','Currency','TimeZones','$stateParams','$state','$http', '$uibModal','$filter', '$mdDialog']
export default function OrdersController(Auth,Order, Shipping, Currency, TimeZones, $stateParams,$state, $http, $uibModal, $filter, $mdDialog) {
  const vm = this
  vm.stateParams = $stateParams
  vm.currency = Currency.getCurrentCurrency()
  vm.timezones = TimeZones.getTimezone()
  vm.timezone = vm.timezones.filter((timezone) => timezone.text === TimeZones.getCurrentTimeZone())[0]
  vm.customers = []
  vm.products = []
  vm.order = {}
  vm.order.order_product = []
  vm.order.products = []
  vm.objProduct = {}
  vm.arrayProduct = []
  vm.currentProductId = 0
  vm.currentQuanity = 1
  vm.asyncCustomer = {}
  vm.asyncProduct= null
  vm.modalProduct = {}
  vm.modalCustomer = {}
  vm.modalProduct.arrayProduct = []
  vm.modalAddress = {}
  vm.modalPromo = []
  vm.totalPrice = 0
  vm.total = 0
  vm.totalPromo = 0
  vm.currentPage = 1
  vm.dateRange  = {startDate: null, endDate: null}
  vm.action = {}
  vm.actions = [{'name':''},
        {'name': 'Verify Payment'}
    ];
  vm.btnSubmit = false
  vm.BtnStatusSubmit = false
  vm.is_billing_shipping = false
  vm.shipping_cost = null
  // vm.propertyName = "id"
  vm.reverse = true
  vm.isPopoverOpen = false
  vm.isVatPopoverOpen = false
  vm.defaultVat = 0
  vm.cancel = false
  vm.shipping_zone_based_rate = []

  vm.popOverStatus  = () => {
    vm.isPopoverOpen = (vm.isPopoverOpen === false) ? true : false;
  }
  vm.popOverVatStatus  = () => {
    vm.isVatPopoverOpen = (vm.isVatPopoverOpen === false) ? true : false;
  }
  vm.setShippingCost = (id) => {
    // vm.shipping_cost = parseFloat(cost)
    // vm.popOverStatus()


    if(id == 2){
      vm.shipping_cost = null
    }
    // vm.tmp_vat_cost = false

  }
  vm.setVat = (boolean) =>{
    if(boolean === false){
      vm.defaultVat = 0
      vm.vat = 0
    }
    vm.defaultVat = (boolean === true) ? (vm.tax_setting ? vm.tax_setting.rate : 0 ) : 0
    
    vm.popOverVatStatus()
  }
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName
  };
  vm.getOrders = function(){

     Order.order(data => {
      vm.orders = data.results
    })
  }

  vm.getDynamicValues = function(){

    Order.dynamic_values(data => {
      vm.dynamic_values = data.results
    })
  }

  vm.stringToFloat = (value) => {
    return parseFloat(value)
  }
  vm.submitForm = function(order) {
    vm.loadedFileName = order.someFile.name;
  };

  vm.newOrder = function(){
    vm.btnSubmit = true
    vm.error = null
     vm.order.customer_id = null
    if(vm.asyncCustomer!= null && vm.asyncCustomer.id !=null ){
      vm.order.customer_id = vm.asyncCustomer.id
      vm.order.email = vm.asyncCustomer.email
    }
    if(vm.asyncCustomer.customer_billing_address){
      vm.order.billing_address = vm.asyncCustomer.customer_billing_address.street_address
      vm.order.billing_country = vm.asyncCustomer.customer_billing_address.country
      vm.order.billing_city = vm.asyncCustomer.customer_billing_address.city
      vm.order.billing_zip_code = vm.asyncCustomer.customer_billing_address.zip_code
    }
    if(vm.asyncCustomer.customer_shipping_address){
      vm.order.shipping_address = vm.asyncCustomer.customer_shipping_address.street_address
      vm.order.shipping_country = vm.asyncCustomer.customer_shipping_address.country
      vm.order.shipping_city = vm.asyncCustomer.customer_shipping_address.city
      vm.order.shipping_zip_code = vm.asyncCustomer.customer_shipping_address.zip_code
    }
    vm.order.promo_order = vm.modalPromo
    vm.order.grand_total = vm.total - vm.totalPromo < 0 ? 0 : vm.total - vm.totalPromo;
    vm.order.totalPromo = vm.totalPromo
    vm.order.vat = vm.vat
    vm.order.shipping_zone_based_rate_id = vm.shipping_cost ? vm.shipping_cost.id : null
    vm.order.tax_setting_id = vm.tax_setting ? vm.tax_setting.id : null
    Order.newOrder(vm.order,data =>{
      $state.go('orders',{CreateOrder : data.results.id},{reload: true});
      vm.btnSubmit = false
    },error =>{
      if (error.data.errors.message) {
        vm.error_message = error.data.errors.message
        vm.btnSubmit = false
      } else {
        vm.error = error.data.errors
        vm.btnSubmit = false
      }

    })
  }
  vm.getOrder = function() {
    Order.show({id: $stateParams.id },data => {
      vm.getShippingZones(true)
      vm.order = data.results
      vm.asyncCustomer = vm.order.customer
      if(!vm.asyncCustomer){
        vm.asyncCustomer = {}
      }
      vm.asyncCustomer.customer_shipping_address = {}
      vm.asyncCustomer.customer_billing_address ={}
      vm.asyncCustomer.customer_billing_address.street_address = vm.order.billing_address
      vm.asyncCustomer.customer_billing_address.country = vm.order.billing_country
      vm.asyncCustomer.customer_billing_address.city = vm.order.billing_city
      vm.asyncCustomer.customer_billing_address.zip_code = vm.order.billing_zip_code
      vm.asyncCustomer.customer_shipping_address.street_address = vm.order.shipping_address
      vm.asyncCustomer.customer_shipping_address.country = vm.order.shipping_country
      vm.asyncCustomer.customer_shipping_address.city = vm.order.shipping_city
      vm.asyncCustomer.customer_shipping_address.zip_code = vm.order.shipping_zip_code
      vm.shipping_cost = vm.order.shipping_zone_based_rate ? vm.order.shipping_zone_based_rate.id : null
      vm.tax_setting = vm.order.tax_setting ? vm.order.tax_setting : null
      if(vm.tax_setting){
        vm.setVat(true)
      }
      if(vm.order.promo_order.length > 0){
        angular.forEach(vm.order.promo_order, function(promo){
             vm.modalPromo.push(promo.promo)
        })

      }
      angular.forEach(vm.order.order_product, function(product) {
          if(new Date(product.product.discount_date_start) <= new Date() && new Date(product.product.discount_date_end) >= new Date()){
            vm.totalPrice += ((product.product.price - product.product.discounted_price) * product.quantity > 0) ? (product.product.price - product.product.discounted_price) * product.quantity : 0

          }else {
            vm.totalPrice += product.product.price * product.quantity
          }
          // vm.totalPrice += product.product.price * product.quantity
          vm.total =  vm.totalPrice

      });
      vm.computePromo(vm.modalPromo)
      getOrderStatus()


    })
  }

  let getOrderStatus = () => {
    Order.orderStatus(data => {
      vm.statuses = data.results
      angular.forEach(vm.statuses, function(status) {
        switch (status.name) {
          case "cart":
            vm.cart = status.id
            break;
          case "pending":
            vm.pending = status.id
            break;
          case "paid":
            vm.paid = status.id
            break;
          case "processing":
            vm.processing = status.id

        }
      })
    });
  }

  vm.showConfirm = (ev) => {
    var confirm = $mdDialog.confirm()
          .title('Cancel Order')
          .textContent('Would you like to cancel this order?')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Yes')
          .cancel('No');

    var already_cancel = $mdDialog.confirm()
          .title('Cancel Order')
          .textContent('Order has been cancelled')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Close')

    $mdDialog.show(confirm).then(function() {
      vm.cancel = true

      Order.cancelOrder(vm.order,data =>{
        $mdDialog.show(already_cancel)
        $state.go('orders.show', {id: data.results.id}, { reload: true });
        vm.cancel = false
         
      })
    }, function() {
      // vm.status = 'No';
      $state.go('orders.show', {id: data.results.id}, { reload: true });
    }

    );


  }

  vm.updateOrder = function(){
    vm.btnSubmit = true
    vm.order.customer_id = vm.asyncCustomer.id
    vm.order.email = vm.asyncCustomer.email
    vm.order.payment_transaction_id = 1
    vm.order.billing_address = vm.asyncCustomer.customer_billing_address.street_address
    vm.order.billing_country = vm.asyncCustomer.customer_billing_address.country
    vm.order.billing_city = vm.asyncCustomer.customer_billing_address.city
    vm.order.billing_zip_code = vm.asyncCustomer.customer_billing_address.zip_code
    vm.order.shipping_address = vm.asyncCustomer.customer_shipping_address.street_address
    vm.order.shipping_country = vm.asyncCustomer.customer_shipping_address.country
    vm.order.shipping_city = vm.asyncCustomer.customer_shipping_address.city
    vm.order.shipping_zip_code = vm.asyncCustomer.customer_shipping_address.zip_code
    vm.order.grand_total = vm.total - vm.totalPromo < 0 ? 0 : vm.total - vm.totalPromo
    vm.order.shipping_zone_based_rate_id = vm.shipping_cost ? vm.shipping_cost : null
    vm.order.promo_order = vm.modalPromo
    vm.isSaving = true
    Order.update(vm.order,data =>{
       $state.go('orders.show', {id: data.results.id}, { reload: true });
       vm.btnSubmit = false
    },error =>{
      if (error.data.errors.message) {
        vm.error_message = error.data.errors.message
        vm.btnSubmit = false
          vm.isSaving = false
      } else {
        vm.error = error.data.errors
        vm.btnSubmit = false
          vm.isSaving =false
      }
    })
  }

  vm.changeStatus = function(order_id) {
    vm.BtnStatusSubmit = true
    vm.order.order = {}
    vm.order.order.id = order_id
    if (vm.checkedEmail == false) {
      vm.email = null
    }
    vm.order.order.email = vm.email
    Order.chageStatus(vm.order.order,data=>{
      vm.order.order_history.order_status.name = data.results.order_history.order_status.name
      vm.order.order_history.order_status.id = data.results.order_history.order_status.id

      vm.BtnStatusSubmit = false
      console.log("tangina")
      console.log(data.results.id)
      $state.go('orders.show', {id: data.results.id}, { reload: true });

    })
  }

  vm.deleteOrder = function(order){
    Order.delete({id: order.id},data=> {
      vm.orders.splice( vm.orders.indexOf(order), 1 );
    })
  }
  vm.getCustomers = function(){
    vm.customers = []
    Order.getCustomer(data=>{
      data.results.map(function(customers){
        vm.customers.push(customers)
      })
    })
  }
  vm.getProducts = function(){
    vm.products = []
    Order.getProducts(data=>{
      data.results.map(function(products){
        vm.products.push(products)
        vm.modalProduct.products = vm.products
      })
    })
  }

  vm.getShippingZones = (isEdit) =>{
    Shipping.index_shiping({id : Auth.getGeneralSettingId()}, data => {
      vm.shipping_setting = data.results
      Shipping.index({id : data.results.id}, data2 => {
        vm.shippings = data2.results
        vm.CheckAndAddShippingZone(vm.shippings,isEdit)
      })
    })
  }

  vm.changeShippingMethod = () =>{
     vm.order.shipping_zone_based_rate = null
     vm.shipping_cost = null
  }

  vm.changeShippingZone = (id) =>{
    const ship = vm.shipping_zone_based_rate.filter((shipping) => shipping.id === id)[0]
    ship ? vm.order.shipping_zone_based_rate = ship : vm.order.shipping_zone_based_rate = null
  }

  vm.CheckAndAddShippingZone = (shippings,isEdit) => {
    // console.log(shippings)
    vm.tax_setting = null
    vm.shipping_zone_based_rate.length = 0
    if(vm.asyncCustomer.customer_shipping_address){
       // console.log(vm.asyncCustomer.customer_shipping_address.country)
      angular.forEach(shippings, function(shipping_zone) {
        const zone = shipping_zone.shipping_zone_country.filter((zone) => zone.country === vm.asyncCustomer.customer_shipping_address.country)[0]
        vm.tax_setting = zone.tax_setting
        isEdit ? vm.tmp_vat_cost = true : vm.tmp_vat_cost = false
        if(zone){
          angular.forEach(shipping_zone.shipping_zone_based_rate, function(based_rate) {
            vm.shipping_zone_based_rate.push(based_rate)
          })
        }
      })
    }
    
  }

  vm.addProductToList = function(){
    if(vm.asyncProduct!=null){
      vm.objProduct.product_id  = vm.asyncProduct.id
      vm.objProduct.name  = vm.asyncProduct.name
      vm.objProduct.sku  = vm.asyncProduct.sku
      vm.objProduct.price = vm.asyncProduct.price
      vm.objProduct.quantity = 1
      vm.arrayProduct.push(vm.objProduct)
      vm.order.products = vm.arrayProduct
      // p = vm.arrayProduct
      vm.objProduct = null
      vm.asyncProduct = null
    }
  }
  vm.addOrderProductToList = function(){
    if(vm.asyncProduct!=null){
      vm.asyncProduct.product = {}
      vm.objProduct.product = {}
      vm.objProduct.product_id  = vm.asyncProduct.id
      vm.objProduct.product.name  = vm.asyncProduct.name
      vm.objProduct.product.sku  = vm.asyncProduct.sku
      vm.objProduct.product.price = vm.asyncProduct.price
      vm.order.order_product.push(vm.objProduct)
      // p = vm.arrayProduct
      vm.objProduct = null
      vm.asyncProduct = null
    }
  }
  //actions
  vm.removeProductOrder = (product) =>{
    vm.order.products.splice(vm.order.products.indexOf(product), 1 );
    vm.computeNewPrice(vm.order.products)
  }
  vm.removeEditProductOrder = (product) =>{
    vm.order.order_product.splice(vm.order.order_product.indexOf(product), 1 );
    vm.computeEditPrice(vm.order.order_product)
  }
  //modals
  vm.openProductModal = () => {
    openProductModalForm()
  }
  vm.openCustomerModal = () =>{
    openCustomerModalForm()
  }
  vm.openAddressModal = () => {
    openAddressModalForm()
  }

  vm.openPromoModal = () => {
    openPromoModalForm()
  }

  let openProductModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/order/templates/order_product_modal.jade'),
      controller: 'OrderProductModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalProduct: function () {
          vm.getProducts()
          return vm.modalProduct
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
        if(vm.modalProduct.arrayProduct.length > 0){
           // vm.order.products = vm.modalProduct.arrayProduct
          angular.forEach(vm.modalProduct.arrayProduct, function(product) {
            vm.newModal = {}
            vm.newModal.product = {}
            vm.newModal.product.name  = product.name
            vm.newModal.product.sku  = product.sku
            vm.newModal.product.price = product.price
            vm.newModal.quantity = 1
            vm.newModal.product_id = product.product_id
            vm.newModal.product.product_categories = product.product_categories
            vm.newModal.product.available_quantity = product.available_quantity
            if(!vm.order.order_product.filter((prod) => prod.sku === product.sku)[0]){
              vm.order.order_product.push(vm.newModal)
            }
            vm.newModal = null
            if(angular.isArray(vm.order.products) && !vm.order.products.filter((prod) => prod.sku === product.sku)[0]){
              vm.order.products.push(product)
            }
          });
        }
        vm.modalProduct.arrayProduct.length = 0
        vm.computePromo(vm.modalPromo)
      }
    )
  }

  let openCustomerModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/order/templates/order_customer_modal.jade'),
      controller: 'CustomerModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalCustomer: function () {
          return vm.modalCustomer
        }
      }
    })

    modalInstance.result.then(
      function (data) {
      },
      function () {
       if(vm.modalCustomer.newCustomers != null){
         vm.asyncCustomer = vm.modalCustomer.newCustomers
       }
      }
    )
  }

  let openAddressModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/order/templates/order_address_modal.jade'),
      controller: 'OrderAddressModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalAddress: function () {
          return vm.asyncCustomer
        }
      }
    })

    modalInstance.result.then(
      function (data) {
      },
      function () {
        vm.CheckAndAddShippingZone(vm.shippings)
      }
    )
  }
  let openPromoModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/app/order/templates/order_promo_modal.jade'),
      controller: 'OrderPromoModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalPromo: function () {
          return vm.modalPromo
        }
      }
    })

    modalInstance.result.then(
      function (data) {
      },
      function () {
        vm.computePromo(vm.modalPromo)
      }
    )
  }

  vm.computePromo = (promos) =>{
    vm.totalPromo = 0
    var products = null
    if(angular.isArray(vm.order.products)){
      products = vm.order.products
    }else{
      products =  vm.order.order_product
    }
    angular.forEach(promos, function(promo) {
          promo.amount_promo = 0
            if(promo.promo_type_id == 3){
                if(promo.amount_type_id == 2){
                  vm.totalPromo += promo.value
                  promo.amount_promo += promo.value
                }
                if(promo.amount_type_id == 1){
                  vm.totalPromo += vm.total * (promo.value / 100)
                  promo.amount_promo += vm.total * (promo.value / 100)
                }
            }

            //products
             if(promo.promo_type_id == 1){
               var categories = null
              angular.forEach(products, function(product){
                if(angular.isArray(vm.order.products)){
                  categories = product.product_categories
                }else{
                  categories = product.product.product_categories
                }
                angular.forEach(categories,function(category){
                  var filter = $filter('filter')(promo.promo_selected, {selected_id: category.category_id})[0]
                  if(filter){
                    if(promo.amount_type_id == 2){
                      vm.totalPromo += promo.value * product.quantity
                      promo.amount_promo += promo.value  * product.quantity
                    }
                    if(promo.amount_type_id == 1){
                      if(angular.isArray(vm.order.products)){
                        vm.totalPromo += (product.price * product.quantity) * (promo.value / 100)
                        promo.amount_promo += (product.price * product.quantity) * (promo.value / 100)
                      }else{
                        vm.totalPromo += (product.product.price * product.quantity) * (promo.value / 100)
                        promo.amount_promo += (product.product.price * product.quantity) * (promo.value / 100)
                      }
                    }
                  }
                })
              })

            }
            if(promo.promo_type_id == 2){
              angular.forEach(products, function(product){
                  var filter = $filter('filter')(promo.promo_selected, {selected_id: product.product_id})[0]
                  if(filter){
                    if(promo.amount_type_id == 2){
                      vm.totalPromo += promo.value * product.quantity
                      promo.amount_promo += promo.value  * product.quantity
                    }
                    if(promo.amount_type_id == 1){
                      if(angular.isArray(vm.order.products)){
                        vm.totalPromo += (product.price * product.quantity) * (promo.value / 100)
                        promo.amount_promo += (product.price * product.quantity) * (promo.value / 100)
                      }else{
                        vm.totalPromo += (product.product.price * product.quantity) * (promo.value / 100)
                        promo.amount_promo += (product.product.price * product.quantity) * (promo.value / 100)
                      }
                    }
                  }
              })

            }
        });
  }

  vm.computeNewPrice = (product) => {
    vm.total = 0
    angular.forEach(product, function(pro) {
        vm.total += pro.price * pro.quantity
    });
    vm.computePromo(vm.modalPromo)
    return vm.total
  }
  vm.computeEditPrice = (product) => {
    vm.total = 0
    angular.forEach(product, function(pro) {
      if(new Date(pro.product.discount_date_start) <= new Date() && new Date(pro.product.discount_date_end) >= new Date()){
          vm.total += ((pro.product.price - pro.product.discounted_price) * pro.quantity > 0) ? (pro.product.price - pro.product.discounted_price) * pro.quantity : 0
      }else{
        vm.total += pro.product.price * pro.quantity
      }
    });
    vm.computePromo(vm.modalPromo)
    return vm.total
  }

  vm.selectAll = function() {
      angular.forEach(vm.orders, function(order) {
        order.Selected = vm.selectedAll;
      });
    };

  vm.checkIfAllSelected = function() {
      vm.selectedAll = vm.orders.every(function(order) {
        return order.Selected == true
      })
    };

  vm.SelectAction = function(){
    if(vm.action == 'Verify Payment'){
      angular.forEach(vm.orders , function(order) {
        if(order.order_history.order_status.name!='pending'){
          order.Selected = false
        }
      })
    }else {
       angular.forEach(vm.orders, function(order) {
        return order.Selected = false;
      });
    }
  }

  vm.SendAction = function(){
    if(vm.action == 'Verify Payment'){
      angular.forEach(vm.orders , function(order) {
        if(order.Selected == true && order.order_history.order_status.name=='pending' ){
          vm.order.order = {}
          vm.order.order.id = order.id
          Order.chageStatus(vm.order.order,data=>{
            order.order_history.order_status.name = data.results.order_history.order_status.name
          })
        }
      })
    }
  }
  vm.removePromo = function(promo){
      vm.modalPromo.splice( vm.modalPromo.indexOf(promo), 1 )
      vm.computePromo(vm.modalPromo)
  }

  vm.openExportModal = () => {
    openExportOrderModalForm()
  }

  let openExportOrderModalForm = () => {
    let modalInstance = $uibModal.open({
      template: require('js/common/templates/_export_modal.jade'),
      controller: 'ExportOrderModal',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        modalOrder: function () {
          return vm.modalOrder
        }
      }
    })

    modalInstance.result.then(
      function () {
      },
      function () {
       vm.getOrders()
      }
    )
  }
}
