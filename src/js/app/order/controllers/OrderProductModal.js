OrderProductModal.$inject = ['Order','Currency','$stateParams','$state', '$uibModalInstance','modalProduct']
export default function OrderProductModal(Order,Currency,$stateParams,$state, $uibModalInstance, modalProduct) {
  	const vm = this
	vm.currentPage = 1
  	vm.modalProduct = modalProduct
  	vm.currency = Currency.getCurrentCurrency()
  	console.log(vm.modalProduct)
	vm.cancel = function () {
    $uibModalInstance.dismiss();
  	};

  	vm.addProductToOrder = function(){
		angular.forEach(vm.modalProduct.products, function(product) {
	        if(product.Selected == true){
        		vm.objProduct = {}
	            vm.objProduct.product_id  = product.id
		    	vm.objProduct.name  = product.name
			    vm.objProduct.sku  = product.sku
			    vm.objProduct.price = product.price
			    if(new Date(product.discount_date_start) <= new Date() && new Date(product.discount_date_end) >= new Date()){
		    		vm.objProduct.price = (product.price - product.discounted_price > 0) ? product.price - product.discounted_price : 0
		    	}
			    vm.objProduct.quantity = 1
			    vm.objProduct.product_categories = product.product_categories
			    vm.objProduct.available_quantity = product.quantity
		    	vm.modalProduct.arrayProduct.push(vm.objProduct)
		    	console.log(product)
		    	vm.objProduct = null

		    	
	        }
     	})
     	$uibModalInstance.dismiss();
  	}
}
