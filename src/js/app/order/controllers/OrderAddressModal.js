OrderAddressModal.$inject = ['Order','StaticData','$stateParams','$state', '$uibModalInstance','modalAddress'] 
export default function OrderAddressModal(Order,StaticData, $stateParams,$state, $uibModalInstance, modalAddress) {
  	const vm = this

  	vm.asyncCustomer = modalAddress
    StaticData.setCountries();
    vm.countries = StaticData.getCountries();
    vm.BillingPlace = null;
    vm.ShippingPlace = null;
    if(!angular.isUndefined(vm.asyncCustomer.customer_shipping_address)){
      vm.ShippingPlace = {name: vm.asyncCustomer.customer_shipping_address.country};
    }
    if(!angular.isUndefined(vm.asyncCustomer.customer_billing_address)){
      vm.BillingPlace = {name: vm.asyncCustomer.customer_billing_address.country};
    }
	vm.cancel = function () {
    	$uibModalInstance.dismiss();
	};

	vm.edit = function () {
		$uibModalInstance.dismiss();
	}
	

}

  