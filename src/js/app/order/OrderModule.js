import OrdersController from './controllers/OrdersController'
import OrderProductModal from './controllers/OrderProductModal'
import OrderAddressModal from './controllers/OrderAddressModal'
import OrderPromoModal from './controllers/OrderPromoModal'
import ExportOrderModal from './controllers/ExportOrderModal'
angular
  .module('order', [])
  .controller('OrdersController', OrdersController)
  .controller('OrderProductModal', OrderProductModal)
  .controller('OrderAddressModal', OrderAddressModal)
  .controller('OrderPromoModal',OrderPromoModal)
  .controller('ExportOrderModal', ExportOrderModal)
  .filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
  })
  .filter('num', function() {
    return function(input) {
      return parseFloat(input, 20);
    }
  })
  .filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
      return $filter('number')(input * 100, decimals) + '%';
    }
  }])
