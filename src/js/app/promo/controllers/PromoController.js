PromoController.$inject = ['Promo', '$stateParams', '$state', '$uibModal', '$http']
export default function PromoController(Promo, $stateParams, $state, $uibModal, $http) {
  const vm = this
  vm.cnt = 0
  vm.currentPage = 1
  vm.products = []
  vm.promo = {}
  vm.categories = []
  vm.reverse = true
  vm.promo.selected = []
  vm.promoitems = []
  vm.categoryCheck = false
  vm.updateditems = []
  vm.promoObject = {}
  vm.percent = false 
  vm.fixed = false
  vm.actions = [
        {'name': 'Action'},
        {'name': 'Delete'}
  ];
  vm.sortBy = function(propertyName) {
      vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
  };

    // vm.modalProduct.arrayProduct = []
  vm.all = () => {
    Promo.all(data => {
      vm.promos = data.results
    }

    );

  }

  vm.getProducts = () => {
    Promo.allProducts(data => {
      vm.products = data.results
    });
  }

  vm.getCategories = () => {
    Promo.allCategories(data => {
      vm.categories = data.results
    });
  }

  vm.getProductType = () => {
    Promo.allPromoTypes(data => {
      vm.promotypes = data.results
    });
  }

  vm.getAmountType = () => {
    Promo.allAmountTypes(data => {
      vm.amounttypes = data.results
      console.log(vm.amounttypes)
      angular.forEach(vm.amounttypes, function (type) {
      if (type.name == 'Percentage') {
        if (vm.promo.amount_type_id == type.id) { 
          vm.percent = true 
          vm.fixed = false
        }
      } else if (type.name == 'Fixed Price') {
        if (vm.promo.amount_type_id == type.id) { 
          vm.fixed = true 
          vm.percent = false
        } 
      }
    })
    });
  }

  vm.change_amount_type = () => {
    console.log(vm.promo.amount_type_id)
    angular.forEach(vm.amounttypes, function (type) {
      if (type.name == 'Percentage') {
        if (vm.promo.amount_type_id == type.id) { 
          vm.percent = true 
          vm.fixed = false
        }
      } else if (type.name == 'Fixed Price') {
        if (vm.promo.amount_type_id == type.id) { 
          vm.fixed = true 
          vm.percent = false
        } 
      }
    })
    
  }

  vm.createPromo = () => {

    angular.forEach(vm.promoitems, function(p) {
      vm.promo.selected.push(p.id)
    });
    Promo.create(vm.promo,
      data => {
        $state.go('promo.show', {id: data.results.id}, { reload: true });
      }, error => {
        vm.errors = error.data.errors
      }
    )
  }

  vm.showPromo = () => {
    vm.updateitems = []
    Promo.show({
      id: $stateParams.id
    }, data => {
      vm.promo = data.results
      if (vm.promo.promo_type_id == 1) {
        Promo.allCategories(data => {
          vm.allcategories = data.results
          angular.forEach(vm.promo.promo_selected, function(promo) {
            angular.forEach(vm.allcategories, function (category) {
              if (promo.selected_id == category.id) {
                vm.promoitems.push(category)
              }
            })
          });
        });


      } else if (vm.promo.promo_type_id == 2){
        Promo.allProducts(data => {
          vm.allproducts = data.results
          angular.forEach(vm.promo.promo_selected, function(promo) {
            angular.forEach(vm.allproducts, function (product) {
              if (promo.selected_id == product.id) {
                vm.promoitems.push(product)
              }
            })
          });
        });
      }

    });
  }

  vm.updatePromo = () => {
    angular.forEach(vm.promoitems, function(p) {
      vm.updateditems.push(p.id)
    });
    vm.promo.selected = vm.updateditems
    console.log(vm.promo.selected)
    Promo.update(vm.promo,
     data =>{
       $state.go('promo.show', {id: data.results.id}, { reload: true });
    },
    error => {
     vm.errors = error.data.errors
    })
  }

  vm.promoOptions = () => {
    vm.promoitems = null;

  }

  vm.deletePromo = (promo) => {
    Promo.delete({id: promo.id},data=> {
      vm.promos.splice( vm.promos.indexOf(promo), 1 );
    },error =>{
      vm.error = error.data.errors
    })
  }


  vm.multipleDelete = () => {
    if(vm.action == 'Delete'){
      vm.promoObject.promo = {}
      vm.promoObject.promo.id = []
      angular.forEach(vm.promos, function(p) {
        if(p.Selected == true){
          console.log("yes");
          vm.promoObject.promo.id.push(p.id)

        }
      })
      Promo.multipleDelete(vm.promoObject ,data=> {
        vm.all()
      })
    }
  }

  vm.selectAll = () => {
      angular.forEach(vm.promos, function(promo) {
        promo.Selected = vm.selectedAll;
      });
    };

  vm.checkIfAllSelected = () => {
      vm.selectedAll = vm.promos.every(function(promo) {
        return promo.Selected == true
      })
    };


}
