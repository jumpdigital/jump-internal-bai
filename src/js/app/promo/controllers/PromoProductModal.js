PromoProductModal.$inject = ['Promo','$stateParams','$state', '$uibModalInstance','modalProduct']
export default function PromoProductModal(Promo,$stateParams,$state, $uibModalInstance, modalProduct) {
  const vm = this
	vm.currentPage = 1
  vm.modalProduct = modalProduct

	vm.cancel = function () {
    console.log('ana')
    $uibModalInstance.dismiss();
  };

	vm.addProductToPromo = () => {
    //console.log(vm.promo.products)
  	angular.forEach(vm.modalProduct.products, function(product) {
          if(product.Selected == true){
        		vm.objProduct = {}
            vm.objProduct.product_id  = product.id
  	    	  vm.objProduct.name  = product.name
  			    vm.objProduct.sku  = product.sku
  		    	vm.modalProduct.arrayProduct.push(vm.objProduct)
  		    	vm.objProduct = null
          }
     })
     $uibModalInstance.dismiss();
	}
}
