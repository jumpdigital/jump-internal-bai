PromoCategoryModal.$inject = ['Promo','$stateParams','$state', '$uibModalInstance','modalCategory']
export default function PromoCategoryModal(Promo,$stateParams,$state, $uibModalInstance, modalCategory) {
  const vm = this
	vm.currentPage = 1
  vm.modalCategory = modalCategory

	vm.cancel = function () {
    console.log('ana')
    $uibModalInstance.dismiss();
  };

	vm.addCategoryToPromo = () => {
    console.log('ana')
  	angular.forEach(vm.modalCategory.categories, function(category) {
          if(category.Selected == true){
        		vm.objCategory = {}
            vm.objCategory.category_id = category.id
  	    	  vm.objCategory.name  = category.name
  		    	vm.modalCategory.arrayCategory.push(vm.objCategory)
  		    	vm.objCategory = null
          }
     })
     $uibModalInstance.dismiss();
	}
}
