import PromoController from './controllers/PromoController'
import PromoProductModal from './controllers/PromoProductModal'
import PromoCategoryModal from './controllers/PromoCategoryModal'

angular
  .module('promo', [])
  .controller('PromoController', PromoController)
  .controller('PromoProductModal', PromoProductModal)
  .controller('PromoCategoryModal', PromoCategoryModal)
