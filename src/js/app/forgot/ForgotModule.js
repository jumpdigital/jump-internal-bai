import ForgotController from './controllers/ForgotController'

angular
  .module('forgot', [])
  .controller('ForgotController', ForgotController)
