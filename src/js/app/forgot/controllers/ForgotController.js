ForgotController.$inject = [
  '$state',
  'User',
  'Auth',
  'toaster',
]

export default function ForgotController($state, User, Auth, toaster) {
  const vm = this

  vm.user = ''
  vm.forgotError = null
  vm.result = null

  vm.forgot = () => {
    User.forgot(vm.user,data => {
        vm.result = data.results
      },
      data => {
        toaster.pop('error', "Login", data.data.errors.message)
      }
    )


  }

}
