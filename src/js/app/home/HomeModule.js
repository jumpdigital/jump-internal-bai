import HomeController from './dashboard/controllers/HomeController'

angular
  .module('home', [])
  .controller('HomeController', HomeController)
