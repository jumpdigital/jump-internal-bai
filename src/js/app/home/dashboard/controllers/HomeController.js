HomeController.$inject = ['User', 'Order', 'Customer', 'Currency']

export default function HomeController(User, Order, Customer, Currency) {
  const vm = this
  vm.total_orders = 0
  vm.total_sales = 0
  vm.labels = []
  vm.currency = Currency.getCurrentCurrency()

  // User.list({}, data => {
  // 	vm.record = data.results
  // 	console.log(vm.record)
  // })
  vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];

  vm.list = () => {
    var date = new Date();
    var monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
    vm.temp_labels = []
    for(var i = 0; i < 6; i++) {
      vm.temp_labels.push(monthNames[date.getMonth()]);
        // Subtract a month each time
      date.setMonth(date.getMonth() - 1);
    }

    for (var i = vm.temp_labels.length - 1; i >= 0  ; i--) {
      console.log(vm.temp_labels[i]);
      vm.labels.push(vm.temp_labels[i])
    }

    vm. series = ['Series A'];
    Order.graph({}, data => {
      vm.data = data.results
    })
  }

  vm.dashboard = () => {
    var quantity = 0
    Order.orderStatus({}, data => {
      vm.status = data.results
      angular.forEach(vm.status, function(status) {
        switch (status.name) {
          case "completed":
            vm.completed = status.id
            break;
          default:

        }
      })

      Order.order({}, data => {
        vm.orders = data.results
        angular.forEach(vm.orders, function(order) {
          if (order.order_history) {
            if (order.order_history.order_status_id == vm.completed) {
              vm.total_orders ++;
              vm.total_sales += order.grand_total*1;
              angular.forEach(order.order_product, function(product) {
                quantity += product.quantity*1
              })
            }
          }


        })
        vm.ave_basket_size = Math.round(quantity / vm.total_orders)
        Customer.all({}, data => {
          vm.customers = data.results
          vm.total_customer = vm.customers.length
          vm.ave_sales_per_user = Math.round(vm.total_sales / vm.total_customer)
        })

        vm.ave_sales_per_order = Math.round(vm.total_sales / vm.total_orders)

      })
    })
  }

  vm.last_five_orders = () => {
    Order.last_five_orders({}, data => {
      vm.orders_list = data.results
      console.log(vm.orders_list);
    })
  }

}
