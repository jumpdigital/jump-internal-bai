export default function UnitSystem() {

  this.unit_systems = {}
  this.current_unit_system = {}

  // manage campaign
  this.setUnitSystems = () => {
      this.unit_systems  = [
        { key: "Metric System",value: [{display: "Kilogram (kg)", symbol: "kg"}, {display: "Gram (g)", symbol: "g"}] },
        { key: "Imperial System", value: [{display: "Ounce (oz)",symbol: "oz"}, {display : "Pound (lb)",symbol: "lb"}] }
      ]
    }

  this.getUnitSystems = () => {
    return this.unit_systems
  }

  this.setCurrentUnitsystem = (weight_unit) =>{
    this.current_unit_system = weight_unit
  }

  this.getCurrentUnitsystem = () => {
    return this.current_unit_system
  }
}