export default function WhenScrolled() {
  return{

    restrict: 'A',
    // replace: true,
    link: function($scope, element, attrs){
      //  console.log(attrs.whenScrolled);

     var raw = element[0];

      element.bind("scroll", function(){
        if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight){
        //  $scope.loading = true;
         $scope.$apply(attrs.whenScrolled);
        //  console.log(attrs.whenScrolled);
       }
     })
    }
  }
}
