import NavbarTemplate from '../templates/_navbar.jade'
import NavbarController from '../controllers/NavBarController'

export default function NavBar() {
  return {
    restrict: 'E',
    replace: true,
    template: NavbarTemplate,
    controller: NavbarController,
    controllerAs: 'navbar'
  }
}