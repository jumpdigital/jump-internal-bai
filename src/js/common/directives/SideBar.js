import sideBarTemplate from '../templates/_sidebar.jade'
import sideBarController from '../controllers/SideBarController'


export default function SideBar() {
  return {
    restrict: 'E',
    replace: true,
    template: sideBarTemplate,
    controller: sideBarController,
    controllerAs: 'sidebar'
  }
}