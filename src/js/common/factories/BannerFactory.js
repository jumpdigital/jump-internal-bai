Banner.$inject = [
  '$resource'
]

export default function Banner($resource) {
  const url = '/api/banners'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    },
    'getBanner': {
      url: url + '/:id',
      params: {
        id: 'id'
      },
      method: 'GET'
    },
    'update':{
      url: url + '/:id',
      params: {
        id: '@id'
      },
      method: 'PUT',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'delete':{
      params: {id: '@id'},
      url: url + '/:id',
      method: 'DELETE'
    },
    'create':{
      url: url,
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
     'multipleDelete':{
       url: url + "/delete",
       method: 'POST'
     },

  }


  return $resource(url, {}, actions)
}
