User.$inject = [
  '$resource'
]

export default function User($resource) {
  const url = '/api/users/:id'
  const userurl = '/api/users'

  const actions = {
    'list': {
      url: url,
      method: 'GET'
    },
    'getUser': {
      url: url,
      method: 'GET',
      params: {
        id: '@id'
      }
    },

    'create':{
      url: url,
      method: 'POST'
    },

    'update':{
      url: url,
      method: 'PATCH',
      params: {
        id: '@id'
      }
    },

    'destroy': {
      url: url,
      method: 'DELETE',
      params:{
        id: '@id'
      }
    },

    'login': {
      url: url,
      method: 'POST',
      params: {
        id: 'login'
      }
    },
    'getCurrentUser': {
      url: url,
      params: {
        id: 'current'
      }
    },
    'forgot': {
      url: url,
      method: 'PATCH',
      params: {
        operation: 'password'
      }
    },
    'updatePassword': {
      url: url,
      method: 'POST',
      params: {
        operation: 'password'
      }
    },
    'deactivate':{
      url: userurl + '/deactivate/:id',
      params: {
        id: '@id'
      },
      method: 'PUT'
    },
    'import': {
      url: url + "/import",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
     'export': {
      url : url + "/export"
    },
  }

  return $resource(url, {}, actions)
}
