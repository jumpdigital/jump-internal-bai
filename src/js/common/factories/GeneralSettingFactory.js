GeneralSetting.$inject = [
  '$resource'
]

export default function GeneralSetting($resource) {
  const url = '/api/general_settings'

  const actions = {
    'show': {
      url: url+"/:id.json",
      params: {
      	id: 'id'
      }
    },
    'create':{
      url: url,
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'update':{
      params: {id: '@id'},
      url: url+"/:id",
      method:'PUT',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    }
    
  }

  return $resource(url, {}, actions)

}
