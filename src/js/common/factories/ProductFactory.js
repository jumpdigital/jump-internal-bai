Product.$inject = [
  '$resource'
]

export default function Product($resource) {
  const url = '/api'
  const actions = {
    'product': {
      url: url+"/products.json",
      method: 'GET'
    },
    'newProduct':{
      url: url+"/products",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'show': {
      params: {id: '@id'},
      url: url+"/products/:id.json",
    },
    'update':{
      params: {id: '@id'},
      url: url+"/products/:id",
      method:'PUT',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'delete':{
      params: {id: '@id'},
      url: url+"/products/:id",
      method: 'DElETE'
    },
    'multipleDelete':{
      url: url+"/products/delete",
      method: 'POST'
    },
    'product_categories': {
      url : url + "/categories.json"
    },
    'createCategory':{
      url: url+"/categories",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'createSubcategory':{
      url: url+"/sub_categories",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'product_subcategories': {
      url : url + "/sub_categories.json"
    },
    'import_product': {
      url: url + "/products/import",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
     'export_product': {
      url : url + "/products/export"
    },
     'getTags': {
      url : url + "/tags"
    },
  }

  return $resource(url, {} ,  actions )
}
