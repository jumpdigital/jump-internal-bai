export default function Sidebar() {
    return {
      nav: function() {
          return [
            {
              id: 1,
              class: 'fa fa-tag',
              text: 'Dashboard',
              link: 'home()',
              parent: []
            },
            {
              id: 2,
              class: 'fa fa-tag',
              text: 'Orders',
              link: 'orders()',
              parent: []
            },
            {
              id: 3,
              class: 'fa fa-bell',
              text: 'Products',
              link: '.',
              parent: '',
              parent: [{
                  id: 4,
                  class: 'fa fa-bell',
                  text: 'Manage Products',
                  link: 'product()',
                  parent: ''
                },
                {
                  id: 5,
                  class: 'fa fa-bell',
                  text: 'Category',
                  link: 'category_product()',
                  parent: ''
                },
                {
                    id: 6,
                    class: 'fa fa-bell',
                    text: 'Sub-category',
                    link: 'subcategory()',
                    parent: ''
                  },
                ]
            },
            {
              id: 7,
              class: 'fa fa-bell',
              text: 'Customers',
              link: 'customers()',
              parent: []
            },
            {
              id: 8,
              class: 'fa fa-bell',
              text: 'Users',
              link: 'users()',
              parent: []
            },
            {
              id: 9,
              class: 'fa fa-bell',
              text: 'Promo',
              link: 'promo()',
              parent: []
            },
            {
              id: 17,
              class: 'fa fa-bell',
              text: 'Banner',
              link: 'banner()',
              parent: []
            },
            {
              id: 10,
              class: 'fa fa-bell',
              text: 'Reports',
              link: 'report()',
              parent: []
            },
            {
              id: 11,
              class: 'fa fa-bell',
              text: 'Settings',
              link: '.',
              parent: '',
              parent: [{
                  id: 12,
                  class: 'fa fa-bell',
                  text: 'General',
                  link: 'setting.general()',
                  parent: ''
                },
                {
                  id: 13,
                  class: 'fa fa-bell',
                  text: 'Payment',
                  link: 'setting.payment()',
                  parent: ''
                },
                {
                    id: 14,
                    class: 'fa fa-bell',
                    text: 'Checkout',
                    link: 'setting.checkout()',
                    parent: ''
                },
                {
                    id: 15,
                    class: 'fa fa-bell',
                    text: 'Shipping',
                    link: 'setting.shipping()',
                    parent: ''
                },
                {
                    id: 16,
                    class: 'fa fa-bell',
                    text: 'Taxes',
                    link: 'setting.taxes()',
                    parent: ''
                },
                {
                    id: 16,
                    class: 'fa fa-bell',
                    text: 'Notification',
                    link: 'setting.notification()',
                    parent: ''
                },
              ]
            },
          ]
      },
    }
}
