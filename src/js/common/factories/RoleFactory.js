Role.$inject = [
  '$resource'
]

export default function Role($resource) {
  const url = '/api/roles'
  const role = '/api/roles/:id'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    }
  }

  return $resource(url, {}, actions)
}
