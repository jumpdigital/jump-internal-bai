Report.$inject = [
  '$resource'
]

export default function Report($resource) {
  const url = '/api/promos'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    },
  }

  return $resource(url, {}, actions)

}
