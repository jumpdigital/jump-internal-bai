ProductCategory.$inject = [
  '$resource' 
]

export default function ProductCategory($resource) {
  const url = '/api'
  const actions = {
    'categories': {
      url: url+"/categories.json",
      method: 'GET'
    },
    'createCategory':{
      url: url+"/categories",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'show': {
      params: {id: '@id'},
      url: url+"/categories/:id.json",
    },
    'update':{
      params: {id: '@id'},
      url: url+"/categories/:id",
      method:'PUT',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'delete':{
      params: {id: '@id'},
      url: url+"/categories/:id",
      method: 'DElETE'
    },
    'product_categories': {
      url : url + "/categories.json"
    },
    'multipleDelete':{
      url: url+"/categories/delete",
      method: 'POST'
    },
  }

  return $resource(url, {} ,  actions )
}
