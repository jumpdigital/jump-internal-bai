export default function Auth() {
  return {
    getToken: () => window.localStorage.authToken,
    setToken: token => window.localStorage.authToken = token,
    deleteToken: () => delete window.localStorage.authToken,

    getName: () => window.localStorage.name,
    setName: name => window.localStorage.name = name,
    deleteName: () => delete window.localStorage.name,

    getEmail: () => window.localStorage.email,
    setEmail: email => window.localStorage.email = email,
    deleteEmail: () => delete window.localStorage.email,

    getStoreName: () => window.localStorage.storeName,
    setStoreName: storeName => window.localStorage.storeName = storeName,
    deleteStoreName: () => delete window.localStorage.storeName,

    getStoreLogoURL: () => window.localStorage.storeLogoURL,
    setStoreLogoURL: storeLogoURL => window.localStorage.storeLogoURL = storeLogoURL,
    deleteStoreLogoURL: () => delete window.localStorage.storeLogoURL,

    getGeneralSettingId: () => window.localStorage.generalSettingId,
    setGeneralSettingId: general_setting_id => window.localStorage.generalSettingId = general_setting_id,
    deleteGeneralSettingId: () => delete window.localStorage.generalSettingId,

    getCurrentCurrency: () => window.localStorage.currentCurrency,
    setCurrentCurrency: currentCurrency => window.localStorage.currentCurrency = currentCurrency,
    deleteCurrentCurrency: () => delete window.localStorage.currentCurrency,

    getWeightUnit: () => window.localStorage.weightUnit,
    setWeightUnit: weightUnit => window.localStorage.weightUnit = weightUnit,
    deleteWeightUnit: () => delete window.localStorage.weightUnit,

    getCurrentTimeZone: () => window.localStorage.timeZone,
    setCurrentTimeZone: timeZone => window.localStorage.timeZone = timeZone,
    deleteCurrentTimeZone: () => delete window.localStorage.timeZone, 

    getAuthType: () => window.localStorage.authType,
    setAuthType: type => window.localStorage.authType = type,
    deleteAuthType: () => delete window.localStorage.authType,
  }
}
