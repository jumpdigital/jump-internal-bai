Promo.$inject = [
  '$resource'
]

export default function Promo($resource) {
  const url = '/api/promos'
  const prod = '/api/products'
  const promotype = '/api/promo_types'
  const category = '/api/categories'
  const amount = '/api/amount_types'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    },
    'create':{
      url:url,
      method:'POST'
    },
    'show': {
      params: {id: '@id'},
      url: url+"/:id",
      method: 'GET'
    },
    'update': {
      params: {id: '@id'},
      url: url+"/:id",
      method: 'PUT'
    },
    'allProducts':{
      url: prod,
      method: 'GET'
    },
    'allPromoTypes':{
      url: promotype,
      method: 'GET'
    },
    'allCategories':{
      url: category,
      method: 'GET'
    },
    'allAmountTypes':{
      url: amount,
      method: 'GET'
    },
    'delete':{
      params: {id: '@id'},
      url: url+"/:id",
      method: 'DELETE'

    },
    'multipleDelete':{
      url: url+"/delete",
      method: 'POST'
    },
  }

  return $resource(url, {}, actions)

}
