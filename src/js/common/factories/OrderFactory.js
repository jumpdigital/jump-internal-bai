Order.$inject = [
  '$resource'
]

export default function Order($resource) {
  const url = '/api'
  const actions = {
    'getOrder': {
      url: url,
      params: {
        id: 'id'
      },
      method: 'GET'
    },
    'order': {
      url: url+"/orders.json",
      method: 'GET'
    },
    'newOrder':{
      url: url+"/orders",
      method: 'POST',
    },
    'show': {
      params: {id: '@id'},
      url: url+"/orders/:id.json",
    },
    'update':{
      params: {id: '@id'},
      url: url+"/orders/:id",
      method:'PUT'
    },
    'delete':{
      params: {id: '@id'},
      url: url+"/orders/:id",
      method: 'DElETE'
    },
    'getCustomer': {
      url: url+"/customers.json",
      method: 'GET'
    },
    'getProducts':{
      url: url+"/products.json"
    },
     'chageStatus':{
      url: url+"/orders/status",
      method: 'POST',
    },
      'addPromo':{
        url: url+"/orders/promo",
        method: 'POST'
    },
      'getPromos':{
        url: url+"/promos",
        method: 'GET'
      },
      'export_orders': {
      url : url + "/orders/export"
    },
    'dynamic_values': {
      url : url + "/dynamic_values"
    },
    'orderStatus':{
      url : url + "/order_statuses",
      method: 'GET'
    },
    'cancelOrder':{
      url : url + "/orders/cancel",
      method: 'POST'

    },
    'graph':{
      url : url + "/orders/graph",
      method: 'GET'
    },

    'last_five_orders':{
      url : url + "/orders/last_five_orders",
      method: 'GET'
    },
  }

  return $resource(url, {} ,  actions )
}
