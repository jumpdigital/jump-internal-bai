Shipping.$inject = [
  '$resource'
]

export default function Shipping($resource) {
  const url = '/api'

  const actions = {
    'index_shiping':{
      url: url+"/shipping?general_setting_id=:id'"
    },
  	'index':{
  		url: url+"/shipping_zone?shipping_setting_id=:id"
  	},
    'create':{
      url: url+"/shipping_zone",
      method: 'POST'
    },
    'update':{
      params: {id: '@id'},
      url: url+"/shipping_zone/:id",
      method:'PUT'
    },
    'show':{
     url: url+"/shipping_zone/:id.json",
    },
    'show_tax':{
      url: url+"/tax_setting/:id.json"
    },
    'update_tax':{
      params: {id: '@id'},
      url: url+"/tax_setting/:id",
      method:'PUT'
    },
  }

  return $resource(url, {}, actions)
}