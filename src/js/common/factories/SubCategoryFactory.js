SubCategory.$inject = [
  '$resource'
]

export default function SubCategory($resource) {
  const url = '/api/sub_categories'
  const sub = '/api/sub_categories/:id'
  const cat = '/api/categories'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    },
    'create': {
      url: url,
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'show': {
      url: sub,
      params: {
        id: 'id'
      },
      method: 'GET',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'update': {
      url: sub,
      params: {
        id: 'id'
      },
      method: 'PUT',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'delete': {
      url: sub,
      params: {
        id: 'id'
      },
      method: 'DELETE',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
    'multipleDelete':{
      url: url+"/delete",
      method: 'POST'
    },

    'product_categories':{
      url: cat,
      methos: 'GET'
    }

  }


  return $resource(url, {}, actions)

}
