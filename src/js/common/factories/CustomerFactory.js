Customer.$inject = [
  '$resource'
]

export default function Customer($resource) {
  const url = '/api/customers'
  const customer = '/api/customers/:id'

  const actions = {
    'all':{
      url: url,
      method: 'GET'
    },
    'getCustomer': {
      url: customer,
      params: {
        id: 'id'
      },
      method: 'GET'
    },
    'update':{
      url: customer,
      params: {
        id: '@id'
      },
      method: 'PUT'
    },
    'destroy':{
      url: customer,
      params: {
        id: 'id'
      },
      method: 'DELETE'
    },
    'create':{
      url: url,
      method: 'POST'
    },
    'deactivate':{
      url: url + '/deactivate/:id',
      params: {
        id: '@id'
      },
      method: 'PUT'
    },
    'import_customer': {
      url: url + "/import",
      method: 'POST',
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    },
     'export_customer': {
      url : url + "/export"
    },
  }


  return $resource(url, {}, actions)
}
