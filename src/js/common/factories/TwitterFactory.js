Twitter.$inject = [
  '$resource'
]

export default function Twitter($resource) {
  const url = '/api/twitters/collect'
  const all = '/api/twitters/:page'
  const harvesturl = '/api/twitters/harvest'

  const actions = {
    'search': {
      url: url,
      method: 'POST'
    },
    'view': {
      url: all,
      method: 'GET',
      params : {
        page: '@page'
      },
    },
    'harvest': {
      url: harvesturl,
      method: 'POST'
    }
  }

  return $resource(url, {}, actions)
}
