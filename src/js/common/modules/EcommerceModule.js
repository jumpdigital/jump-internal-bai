import HeaderService from '../services/HeaderService'
import HeaderController from '../controllers/HeaderController'
import UsersController from '../../app/users/controllers/UsersController'
import ExportUserModal from '../../app/users/controllers/ExportUserModal'
import ImportUserModal from '../../app/users/controllers/ImportUserModal'

angular
  .module('ecommerce', [])
  .service('HeaderService', HeaderService)
  .controller('HeaderController', HeaderController)
  .controller('UsersController', UsersController)
  .controller('ImportUserModal', ImportUserModal)
  .controller('ExportUserModal', ExportUserModal);
