NavBarController.$inject = [
  '$state',
  'Auth',
]

export default function NavBarController($state, Auth) {
  const vm = this

  vm.title = $state.current.name

  vm.logout = () => {
    Auth.deleteToken()
    Auth.deleteAuthType()
    $state.go('login')
  }



}