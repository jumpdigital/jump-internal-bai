ScrollController.$inject = []
export default function ScrollController() {
  const vm = this

  vm.scrolling = function(elem, attrs){
    var raw = elem[0];
    elem.bind("scroll", function(){
      if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
        vm.loading = true;
        vm.$apply(attrs.whenScrolled);
      }
    });
  }
}
