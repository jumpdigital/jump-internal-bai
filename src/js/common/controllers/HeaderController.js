HeaderController.$inject = [
  'HeaderService'
];

export default function HeaderController(HeaderService) {
  const vm = this;

  vm.page_title = HeaderService.page_title;
  vm.page_icon = HeaderService.page_icon;
}
