SideBarController.$inject = [
  '$state',
  'Auth',
  '$timeout',
  'Sidebar'
]

export default function SideBarController($state, Auth, $timeout, Sidebar) {
  const vm = this
  vm.current_user = Auth.getName()
  vm.isCollapsed = false
  vm.auth = Auth
  vm.openUserDropDown = false
  vm.logout_icon = 'fa-power-off'
  vm.toggleUserDropDown = () => {
  	if(vm.openUserDropDown)
  		vm.openUserDropDown = false
  	else
  		vm.openUserDropDown = true
  }

  vm.logout = () => {
    vm.logout_icon = 'fa-spinner fa-spin'

    $timeout(() => {
    	Auth.deleteToken()
    	Auth.deleteAuthType()
      Auth.deleteGeneralSettingId()
      Auth.deleteEmail()
      Auth.deleteName()
      Auth.deleteCurrentCurrency()
      Auth.deleteStoreLogoURL()
      Auth.deleteStoreName()
      Auth.deleteWeightUnit()
      Auth.deleteCurrentTimeZone()
		$state.go('login')
    }, 1000);
  }

  vm.nav = Sidebar.nav()
 
}