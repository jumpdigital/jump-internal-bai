// Import factories
import Auth from './factories/AuthFactory'
import User from './factories/UserFactory'
import Twitter from './factories/TwitterFactory'
import Role from './factories/RoleFactory'
import Product from './factories/ProductFactory'
import ProductCategory from './factories/ProductCategoryFactory'
import Customer from './factories/CustomerFactory'
import Order from './factories/OrderFactory'
import Sidebar from './factories/SidebarFactory'
import SubCategory from './factories/SubCategoryFactory'
import Promo from './factories/PromoFactory'
import Report from './factories/ReportFactory'
import GeneralSetting from './factories/GeneralSettingFactory'
import Shipping from './factories/ShippingFactory'
import Banner from './factories/BannerFactory'
// import Role from './factories/RoleFactory'
// Import controllers


// Import directives
import HomeSideBar from './directives/SideBar'
import HomeNavBar from './directives/NavBar'
import WhenScrolled from './directives/WhenScrolled'
import SlideBar from './directives/SlideBar'

import StaticData from './services/StaticData'
import TimeZones from './services/TimeZones'
import Currency from './services/Currency'
import UnitSystem from './services/UnitSystem'

angular
  .module('common', [])

  .factory('Auth', Auth)
  .factory('User', User)
  .factory('Twitter', Twitter)
  .factory('Role', Role)
  .factory('Product',Product)
  .factory('ProductCategory', ProductCategory)
  .factory('Customer', Customer)
  .factory('Order', Order)
  .factory('Sidebar', Sidebar)
  .factory('SubCategory', SubCategory)
  .factory('Promo', Promo)
  .factory('Report', Report)
  .factory('GeneralSetting', GeneralSetting)
  .factory('Shipping',Shipping)
  // .factory('Role', Role)
  // component directives
  .directive('homeSidebar', HomeSideBar)
  .directive('homeNavbar', HomeNavBar)
  .directive('whenScrolled', WhenScrolled)
  .directive('slideBar', SlideBar)

  .service('StaticData', StaticData)
  .service('TimeZones', TimeZones)
  .service('Currency', Currency)
  .service('UnitSystem', UnitSystem)
  .service('Banner', Banner)
