  States.$inject = [
  '$urlRouterProvider',
  '$stateProvider',
  '$locationProvider',
  '$injector'
]

export default function States($urlRouterProvider, $stateProvider, $locationProvider, $injector) {
  $stateProvider

    /**
     * home
     */
    .state('home',{
      url: '/',
      templateProvider: [() => require('js/app/home/dashboard/templates/index.jade')],
      controller: 'HomeController as vm',
    })
    .state('login', {
      url: '/login',
      templateProvider: [() => require('js/app/login/templates/index.jade')],
      controller: 'LoginController as vm',
    })
    .state('forgot', {
      url: '/forgot',
      templateProvider: [() => require('js/app/forgot/templates/index.jade')],
      controller: 'ForgotController as vm',
    })
    .state('change-password', {
      url: '/change-password/:token?',
      templateProvider: [() => require('js/app/change-password/templates/edit.html')],
      controller: 'ChangePasswordController as vm',
    })

    /**
    * users
    */
    .state('users', {
      url: '/users',
      templateProvider: [() => require('js/app/users/templates/index.jade')],
      controller: 'UsersController as vm',
    })
    .state('users.new', {
      url: '/new',
      templateProvider: [() => require('js/app/users/templates/new.jade')],
      controller: 'UsersController as vm',
    })
    .state('users.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/users/templates/show.jade')],
      controller: 'UsersController as vm',
    })
    .state('users.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/users/templates/edit.jade')],
      controller: 'UsersController as vm',
    })

    //bai
    .state('product', {
      url: '/product',
      templateProvider: [() => require('js/app/product/templates/index.jade')],
      controller: 'ProductsController as vm',
    })
    .state('product.new', {
      url: '/new',
      templateProvider: [() => require('js/app/product/templates/new.jade')],
      controller: 'ProductsController as vm',
    })
    .state('product.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/product/templates/show.jade')],
      controller: 'ProductsController as vm',

    })
    .state('product.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/product/templates/edit.jade')],
      controller: 'ProductsController as vm',

    })
    //product category
    .state('category_product', {
      url: '/category/product',
      templateProvider: [() => require('js/app/product_category/templates/index.jade')],
      controller: 'ProductCategoryController as vm',
    })
    .state('category_product.new', {
      url: '/new',
      templateProvider: [() => require('js/app/product_category/templates/new.jade')],
      controller: 'ProductCategoryController as vm',

    })
    .state('category_product.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/product_category/templates/show.jade')],
      controller: 'ProductCategoryController as vm',
    })
    .state('category_product.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/product_category/templates/edit.jade')],
      controller: 'ProductCategoryController as vm',
    })


    // customers crud
    .state('customers', {
      url: '/customers',
      templateProvider: [() => require('js/app/customer/templates/index.jade')],
      controller: 'CustomerController as vm',
    })
    .state('customers.new', {
      url: '/new',
      templateProvider: [() => require('js/app/customer/templates/new.jade')],
      controller: 'CustomerController as vm',
    })
    .state('customers.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/customer/templates/show.jade')],
      controller: 'CustomerController as vm',
    })
    .state('customers.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/customer/templates/edit.jade')],
      controller: 'CustomerController as vm',
    })
    .state('orders', {
      url: '/order',
      params:{CreateOrder:null},
      templateProvider: [() => require('js/app/order/templates/index.jade')],
      controller: 'OrdersController as vm',
    })
    .state('orders.new', {
      url: '/new',
      templateProvider: [() => require('js/app/order/templates/new.jade')],
      controller: 'OrdersController as vm',
    })
    .state('orders.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/order/templates/show.jade')],
      controller: 'OrdersController as vm',
    })
    .state('orders.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/order/templates/edit.jade')],
      controller: 'OrdersController as vm',
    })

    // sub_category

    .state('subcategory', {
      url: '/subcategory/product',
      templateProvider: [() => require('js/app/sub_category/templates/index.jade')],
      controller: 'SubCategoryController as vm',
    })
    .state('subcategory.new', {
      url: '/new',
      templateProvider: [() => require('js/app/sub_category/templates/new.jade')],
      controller: 'SubCategoryController as vm',
    })
    .state('subcategory.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/sub_category/templates/show.jade')],
      controller: 'SubCategoryController as vm',
    })
    .state('subcategory.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/sub_category/templates/edit.jade')],
      controller: 'SubCategoryController as vm',
    })

    // promo
    .state('promo', {
      url: '/promo',
      templateProvider: [() => require('js/app/promo/templates/index.jade')],
      controller: 'PromoController as vm',
    })

    .state('promo.new', {
      url: '/new',
      templateProvider: [() => require('js/app/promo/templates/new.jade')],
      controller: 'PromoController as vm',
    })
    .state('promo.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/promo/templates/show.jade')],
      controller: 'PromoController as vm',
    })
    .state('promo.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/promo/templates/edit.jade')],
      controller: 'PromoController as vm',
    })

    //report

    .state('report', {
      url: '/report',
      templateProvider: [() => require('js/app/report/templates/index.jade')],
      controller: 'ReportController as vm',
    })
    .state('setting',{
      url: '/settings',
      redirectTo: 'setting.general',
      templateProvider: [() => require('js/app/setting/templates/general_setting.jade')],
      // controller: 'GeneralSettingController as vm'
    })
    .state('setting.general', {
      url: '/general',
      controller: 'GeneralSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/general_setting.jade')]
    })
    .state('setting.shipping', {
      url: '/shipping',
      templateProvider: [() => require('js/app/setting/templates/shipping_setting.jade')],
      controller: 'ShippingSettingController as vm'
    })
    .state('setting.shipping.new', {
      url: '/new?shipping_setting_id=:id',
      templateProvider: [() => require('js/app/setting/templates/shipping_setting_new.jade')],
      controller: 'ShippingSettingController as vm'
    })
    .state('setting.shipping.edit', {
      url: '/:id',
      params:{CreateShippingZone:null},
      templateProvider: [() => require('js/app/setting/templates/shipping_setting_new.jade')],
      controller: 'ShippingSettingController as vm'
    })
    .state('setting.taxes', {
      url: '/taxes',
      controller: 'TaxSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/taxes_setting.jade')]
    })
    .state('setting.taxes.edit', {
      url: '/:id',
      controller: 'TaxSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/taxes_setting_edit.jade')]
    })
    .state('setting.payment', {
      url: '/payment',
      controller: 'GeneralSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/payment_setting.jade')]
    })
    .state('setting.notification', {
      url: '/notification',
      controller: 'GeneralSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/notification_setting.jade')]
    })
    .state('setting.checkout', {
      url: '/checkout',
      controller: 'GeneralSettingController as vm',
      templateProvider: [() => require('js/app/setting/templates/checkout_setting.jade')]
    })

    // banner

    .state('banner', {
      url: '/banner',
      templateProvider: [() => require('js/app/banner/templates/index.jade')],
      controller: 'BannerController as vm',
    })

    .state('banner.new', {
      url: '/new',
      templateProvider: [() => require('js/app/banner/templates/new.jade')],
      controller: 'BannerController as vm',
    })
    .state('banner.show', {
      url: '/:id',
      templateProvider: [() => require('js/app/banner/templates/show.jade')],
      controller: 'BannerController as vm',
    })
    .state('banner.edit', {
      url: '/:id/edit',
      templateProvider: [() => require('js/app/banner/templates/edit.jade')],
      controller: 'BannerController as vm',
    });



    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    $urlRouterProvider.otherwise('/');
}
