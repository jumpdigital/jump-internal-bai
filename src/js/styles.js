// Import app-wide styles here
import 'ng-table/dist/ng-table.min.css'
import 'angular-datepicker/dist/index.css'
import 'angular-ui-select/dist/select.min.css'
import 'ng-tags-input/ng-tags-input.min.css'
import 'angular-material/angular-material.min.css'
import 'angular-loading-bar/build/loading-bar.min.css'
import 'scss/app.scss'
