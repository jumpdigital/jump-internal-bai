
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var path = require('path');
var merge = require('webpack-merge');

// indicate the script used
var TARGET = process.env.npm_lifecycle_event;

var srcPath = path.join(__dirname, 'src');
var distPath = path.join(__dirname, 'build');

var devServerConfig = {
  hot: true,
  progress: true,
  inline: true,
  historyApiFallback: true,
  stats: { colors: true },

  contentBase: distPath,
  outputPath: distPath,

  watchOptions: {
    poll: true
  },

  host: '0.0.0.0',
  port: 5000,
  proxy: {
    '/api/*': {
      // target: 'http://127.0.0.1:8080',
      target: 'http://localhost:3000',
      changeOrigin: true,
      secure: false,
    }
  }

  // Display only errors to reduce the amount of output.
  // stats: 'errors-only',
};

var commonConfig = {
  target: 'web',

  entry: {
    app: path.join(srcPath, 'js/app.js')
  },

  resolve: {
    root: srcPath,
    extensions: ['', '.js'],
    modulesDirectories: [
      'node_modules',
      'bower_components',
      srcPath
    ]
  },

  output: {
    path: distPath,
    publicPath: '/',
    filename: 'js/[name]-[hash].js',
    chunkFilename: 'js/[id]-[hash].js'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015', "stage-1", "stage-2"]
        }
      },

      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
        loader: 'url?limit=25000',
        include: srcPath
      },

      {
        test: /\.css$/,
        loader: 'style!raw'
      },

      {
        test: /\.scss$/,
        loader: 'style!css!sass'
      },

      {
        test: /\.jade$/,
        loader: 'raw!jade-html'
      },

      {
        test: /\.html$/,
        loader: 'raw'
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(['build']),

    new CopyWebpackPlugin([
      { from: path.join(srcPath, 'img'), to: path.join(distPath, 'img') },
      { from: path.join(srcPath, 'fonts'), to: path.join(distPath, 'fonts') },
      { from: path.join(srcPath, 'partials'), to: path.join(distPath, 'partials') },
    ]),

    new HtmlWebpackPlugin({
      inject: true,
      template: 'src/index.html',
      filename: 'index.html',
    }),

    new webpack.NoErrorsPlugin(),
  ]
};

// Environment-specific configurations
if (TARGET == 'dev' || !TARGET) {
  module.exports = merge(commonConfig, {
    devtool: 'eval-source-map',
    // devtool: 'eval-cheap-module-source-map',           // faster

    plugins: [
      new webpack.HotModuleReplacementPlugin(),
    ],

    devServer: devServerConfig
  });
}else if (TARGET === 'staging') {
  module.exports = merge(commonConfig, {
    plugins: [
      new webpack.HotModuleReplacementPlugin(),

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
    ],

    devServer: devServerConfig
  });
}else if (TARGET === 'build') {
  module.exports = merge(commonConfig, {
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
    ]
  });
}
